




// DO NOT EDIT !
// This file is generated using the MantaFlow preprocessor (prep generate).




#line 1 "/home/zara/Projects/guiding_fluid/manta-src-0.12/manta_0_12/source/plugin/fluidguiding.cpp"
/******************************************************************************
*
* MantaFlow fluid solver framework
* Copyright 2011 Tobias Pfaff, Nils Thuerey
*
* This program is free software, distributed under the terms of the
* Apache License, Version 2.0
* http://www.apache.org/licenses/LICENSE-2.0
*
* Plugins for pressure correction: solve_pressure, and ghost fluid helpers
*
******************************************************************************/
#include "vectorbase.h"
#include "grid.h"
#include "kernel.h"
#include "conjugategrad.h"
#include "rcmatrix.h"
#include <fftw3.h>

using namespace std;
namespace Manta {

// only supports a single blur size for now, globals stored here
    bool gBlurPrecomputed = false;
    int  gBlurKernelRadius = -1;
    Matrix gBlurKernel;

// *****************************************************************************
// Helper functions for fluid guiding

//! creates a 1D (horizontal) Gaussian blur kernel of size n and standard deviation sigma
    Matrix get1DGaussianBlurKernel(const int n, const int sigma) {
        Matrix x(n), y(n);
        for (int j = 0; j < n; j++) {
            x.add_to_element(0, j, - (n - 1)*0.5);
            y.add_to_element(0, j, j - (n - 1)*0.5);
        }
        Matrix G(n);
        Real sumG = 0;
        for (int j = 0; j < n; j++) {
            G.add_to_element(0, j, 1 / (2 * M_PI*sigma*sigma)*exp(-(x(0, j)*x(0, j) + y(0, j)*y(0, j)) / (2 * sigma*sigma)));
            sumG += G(0, j);
        }
        G = G*(1.0 / sumG);
        return G;
    }

//! convolves in with 1D kernel (centred at the kernel's midpoint) in the x-direction
//! (out must be a grid of zeros)
     struct apply1DKernelDirX : public KernelBase { apply1DKernelDirX(const MACGrid &in, MACGrid &out, const Matrix &kernel) :  KernelBase(&in,0) ,in(in),out(out),kernel(kernel)   { runMessage(); run(); }  inline void op(int i, int j, int k, const MACGrid &in, MACGrid &out, const Matrix &kernel )  {
        int nx = in.getSizeX();
        int kn = kernel.n;
        int kCentre = kn / 2;
        for (int m = 0, ind = kn - 1, ii = i - kCentre; m < kn; m++, ind--, ii++) {
            if (ii < 0) continue;
            else if (ii >= nx) break;
            else out(i, j, k) += in(ii, j, k)*kernel(0,ind);
        }
    }   inline const MACGrid& getArg0() { return in; } typedef MACGrid type0;inline MACGrid& getArg1() { return out; } typedef MACGrid type1;inline const Matrix& getArg2() { return kernel; } typedef Matrix type2; void runMessage() { debMsg("Executing kernel apply1DKernelDirX ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; for (int k=minZ; k< maxZ; k++) for (int j=0; j< _maxY; j++) for (int i=0; i< _maxX; i++) op(i,j,k, in,out,kernel);  } const MACGrid& in; MACGrid& out; const Matrix& kernel;   };

//! convolves in with 1D kernel (centred at the kernel's midpoint) in the y-direction
//! (out must be a grid of zeros)
     struct apply1DKernelDirY : public KernelBase { apply1DKernelDirY(const MACGrid &in, MACGrid &out, const Matrix &kernel) :  KernelBase(&in,0) ,in(in),out(out),kernel(kernel)   { runMessage(); run(); }  inline void op(int i, int j, int k, const MACGrid &in, MACGrid &out, const Matrix &kernel )  {
        int ny = in.getSizeY();
        int kn = kernel.n;
        int kCentre = kn / 2;
        for (int m = 0, ind = kn - 1, jj = j - kCentre; m < kn; m++, ind--, jj++) {
            if (jj < 0) continue;
            else if (jj >= ny) break;
            else out(i, j, k) += in(i, jj, k)*kernel(0, ind);
        }
    }   inline const MACGrid& getArg0() { return in; } typedef MACGrid type0;inline MACGrid& getArg1() { return out; } typedef MACGrid type1;inline const Matrix& getArg2() { return kernel; } typedef Matrix type2; void runMessage() { debMsg("Executing kernel apply1DKernelDirY ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; for (int k=minZ; k< maxZ; k++) for (int j=0; j< _maxY; j++) for (int i=0; i< _maxX; i++) op(i,j,k, in,out,kernel);  } const MACGrid& in; MACGrid& out; const Matrix& kernel;   };

//! convolves in with 1D kernel (centred at the kernel's midpoint) in the z-direction
//! (out must be a grid of zeros)
     struct apply1DKernelDirZ : public KernelBase { apply1DKernelDirZ(const MACGrid &in, MACGrid &out, const Matrix &kernel) :  KernelBase(&in,0) ,in(in),out(out),kernel(kernel)   { runMessage(); run(); }  inline void op(int i, int j, int k, const MACGrid &in, MACGrid &out, const Matrix &kernel )  {
        int nz = in.getSizeZ();
        int kn = kernel.n;
        int kCentre = kn / 2;
        for (int m = 0, ind = kn - 1, kk = k - kCentre; m < kn; m++, ind--, kk++) {
            if (kk < 0) continue;
            else if (kk >= nz) break;
            else out(i, j, k) += in(i, j, kk)*kernel(0, ind);
        }
    }   inline const MACGrid& getArg0() { return in; } typedef MACGrid type0;inline MACGrid& getArg1() { return out; } typedef MACGrid type1;inline const Matrix& getArg2() { return kernel; } typedef Matrix type2; void runMessage() { debMsg("Executing kernel apply1DKernelDirZ ", 3); debMsg("Kernel range" <<  " x "<<  maxX  << " y "<< maxY  << " z "<< minZ<<" - "<< maxZ  << " "   , 4); }; void run() {  const int _maxX = maxX; const int _maxY = maxY; for (int k=minZ; k< maxZ; k++) for (int j=0; j< _maxY; j++) for (int i=0; i< _maxX; i++) op(i,j,k, in,out,kernel);  } const MACGrid& in; MACGrid& out; const Matrix& kernel;   };

//! Apply separable Gaussian blur in 2D
    void applySeparableKernel2D(MACGrid &grid, const FlagGrid &flags, const Matrix &kernel) {
        //int nx = grid.getSizeX(), ny = grid.getSizeY();
        //int kn = kernel.n;
        //int kCentre = kn / 2;
        FluidSolver* parent = grid.getParent();
        MACGrid orig = MACGrid(parent);
        orig.copyFrom(grid);
        MACGrid gridX = MACGrid(parent);
        apply1DKernelDirX(grid, gridX, kernel);
        MACGrid gridXY = MACGrid(parent);
        apply1DKernelDirY(gridX, gridXY, kernel);
        grid.copyFrom(gridXY);
        FOR_IJK(grid) {
                    if ((i>0 && flags.isObstacle(i - 1, j, k)) || (j>0 && flags.isObstacle(i, j - 1, k)) || flags.isObstacle(i, j, k)) {
                        grid(i, j, k).x = orig(i, j, k).x;
                        grid(i, j, k).y = orig(i, j, k).y;
                        grid(i, j, k).z = orig(i, j, k).z;
                    }
                }
    }

//! Apply separable Gaussian blur in 3D
    void applySeparableKernel3D(MACGrid &grid, const FlagGrid &flags, const Matrix &kernel) {
        //int nx = grid.getSizeX(), ny = grid.getSizeY(), nz = grid.getSizeZ();
        //int kn = kernel.n;
        //int kCentre = kn / 2;
        FluidSolver* parent = grid.getParent();
        MACGrid orig = MACGrid(parent);
        orig.copyFrom(grid);
        MACGrid gridX = MACGrid(parent);
        apply1DKernelDirX(grid, gridX, kernel);
        MACGrid gridXY = MACGrid(parent);
        apply1DKernelDirY(gridX, gridXY, kernel);
        MACGrid gridXYZ = MACGrid(parent);
        apply1DKernelDirZ(gridXY, gridXYZ, kernel);
        grid.copyFrom(gridXYZ);
        FOR_IJK(grid) {
                    if ((i>0 && flags.isObstacle(i - 1, j, k)) || (j>0 && flags.isObstacle(i, j - 1, k)) || (k>0 && flags.isObstacle(i, j, k - 1)) || flags.isObstacle(i, j, k)) {
                        grid(i, j, k).x = orig(i, j, k).x;
                        grid(i, j, k).y = orig(i, j, k).y;
                        grid(i, j, k).z = orig(i, j, k).z;
                    }
                }
    }

//! Apply separable Gaussian blur in 2D or 3D depending on input dimensions
    void applySeparableKernel(MACGrid &grid, const FlagGrid &flags, const Matrix &kernel) {
        if (!grid.is3D()) applySeparableKernel2D(grid, flags, kernel);
        else applySeparableKernel3D(grid, flags, kernel);
    }


//! Compute r-norm for the stopping criterion
    Real getRNorm(const MACGrid &x, const MACGrid &z) {
        MACGrid r = MACGrid(x.getParent());
        r.copyFrom(x);
        r.sub(z);
        return r.getMaxAbs();
    }

//! Compute s-norm for the stopping criterion
    Real getSNorm(const Real rho, const MACGrid &z, const MACGrid &z_prev) {
        MACGrid s = MACGrid(z_prev.getParent());
        s.copyFrom(z_prev);
        s.sub(z);
        s.multConst(rho);
        return s.getMaxAbs();
    }

//! Compute primal eps for the stopping criterion
    Real getEpsPri(const Real eps_abs, const Real eps_rel,
                   const MACGrid &x, const MACGrid &z) {
        Real max_norm = max(x.getMaxAbs(), z.getMaxAbs());
        Real eps_pri = sqrt(x.is3D() ? 3.0 : 2.0)*eps_abs + eps_rel*max_norm;
        return eps_pri;
    }

//! Compute dual eps for the stopping criterion
    Real getEpsDual(const Real eps_abs, const Real eps_rel, const MACGrid &y) {
        Real eps_dual = sqrt(y.is3D() ? 3.0 : 2.0)*eps_abs + eps_rel*y.getMaxAbs();
        return eps_dual;
    }

//! Create a spiral velocity field in 2D as a test scene
    void getSpiralVelocity2D(const FlagGrid &flags, MACGrid &vel, Real strength = 1.0) {
        int nx = flags.getSizeX(), ny = flags.getSizeY();
        Real midX = 0.5*(Real)(nx - 1);
        Real midY = 0.5*(Real)(ny - 1);
        int k = 0;
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                int idx = flags.index(i, j, k);
                Real diffX = midX - i;
                Real diffY = midY - j;
                Real hypotenuse = sqrt(diffX*diffX + diffY*diffY);
                if (hypotenuse > 0) {
                    vel[idx].x = diffY / hypotenuse;
                    vel[idx].y = -diffX / hypotenuse;
                }
            }
        }
        vel.multConst(strength);
    } static PyObject* _W_0 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "getSpiralVelocity2D" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; const FlagGrid& flags = *_args.getPtr<FlagGrid >("flags",0,&_lock); MACGrid& vel = *_args.getPtr<MACGrid >("vel",1,&_lock); Real strength = _args.getOpt<Real >("strength",2,1.0,&_lock);   _retval = getPyNone(); getSpiralVelocity2D(flags,vel,strength);  _args.check(); } pbFinalizePlugin(parent,"getSpiralVelocity2D", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("getSpiralVelocity2D",e.what()); return 0; } } static const Pb::Register _RP_getSpiralVelocity2D ("","getSpiralVelocity2D",_W_0);  extern "C" { void PbRegister_getSpiralVelocity2D() { KEEP_UNUSED(_RP_getSpiralVelocity2D); } } 


    //! Create a spiral velocity field in 2D as a test scene
    void getCircularVelocity2D(const FlagGrid &flags, MACGrid &vel, Real strength = 1.0) {
        int nx = flags.getSizeX(), ny = flags.getSizeY();
        Real midX = 0.5*(Real)(nx - 1);
        Real midY = 0.5*(Real)(ny - 1);
        int k = 0;
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                int idx = flags.index(i, j, k);
                vel[idx].x = cos(idx*M_PI);
                vel[idx].y = sin(idx*M_PI);

            }
        }
        vel.multConst(strength);
    } static PyObject* _W_1 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "getCircularVelocity2D" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; const FlagGrid& flags = *_args.getPtr<FlagGrid >("flags",0,&_lock); MACGrid& vel = *_args.getPtr<MACGrid >("vel",1,&_lock); Real strength = _args.getOpt<Real >("strength",2,1.0,&_lock);   _retval = getPyNone(); getCircularVelocity2D(flags,vel,strength);  _args.check(); } pbFinalizePlugin(parent,"getCircularVelocity2D", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("getCircularVelocity2D",e.what()); return 0; } } static const Pb::Register _RP_getCircularVelocity2D ("","getCircularVelocity2D",_W_1);  extern "C" { void PbRegister_getCircularVelocity2D() { KEEP_UNUSED(_RP_getCircularVelocity2D); } } 

//! Set the guiding weight W as a gradient in the y-direction
    void setGradientYWeight(Grid<Real> &W, const int minY, const int maxY, const Real valAtMin, const Real valAtMax) {
        FOR_IJK(W) {
                    if (minY <= j && j <= maxY) {
                        Real val = valAtMin;
                        if (valAtMax != valAtMin) {
                            Real ratio = (Real)(j - minY) / (Real)(maxY - minY);
                            val = ratio*valAtMax + (1.0 - ratio)*valAtMin;
                        }
                        W(i, j, k) = val;
                    }
                }
    } static PyObject* _W_2 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "setGradientYWeight" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; Grid<Real> & W = *_args.getPtr<Grid<Real>  >("W",0,&_lock); const int minY = _args.get<int >("minY",1,&_lock); const int maxY = _args.get<int >("maxY",2,&_lock); const Real valAtMin = _args.get<Real >("valAtMin",3,&_lock); const Real valAtMax = _args.get<Real >("valAtMax",4,&_lock);   _retval = getPyNone(); setGradientYWeight(W,minY,maxY,valAtMin,valAtMax);  _args.check(); } pbFinalizePlugin(parent,"setGradientYWeight", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("setGradientYWeight",e.what()); return 0; } } static const Pb::Register _RP_setGradientYWeight ("","setGradientYWeight",_W_2);  extern "C" { void PbRegister_setGradientYWeight() { KEEP_UNUSED(_RP_setGradientYWeight); } } 

// *****************************************************************************
// More helper functions for fluid guiding

//! Apply Gaussian blur (either 2D or 3D) in a separable way
    void applySeparableGaussianBlur(MACGrid &grid, const FlagGrid &flags, const Matrix &kernel1D) {
        assertMsg(gBlurPrecomputed, "Error - blue kernel not precomputed");
        applySeparableKernel(grid, flags, kernel1D);
    }

//! Precomputation performed before the first PD iteration
    void ADMM_precompute_Separable(int blurRadius) {
        if (gBlurPrecomputed) {
            assertMsg( gBlurKernelRadius == blurRadius, "More than a single blur radius not supported at the moment." );
            return;
        }
        int kernelSize = 2 * blurRadius + 1;
        gBlurKernel = get1DGaussianBlurKernel(kernelSize, kernelSize);
        gBlurPrecomputed = true;
        gBlurKernelRadius = blurRadius;
    }

//! Apply approximate multiplication of inverse(M)
    void applyApproxInvM(MACGrid& v, const FlagGrid &flags, const MACGrid& invA) {
        MACGrid v_new = MACGrid(v.getParent());
        v_new.copyFrom(v);
        v_new.mult(invA);
        applySeparableGaussianBlur(v_new, flags, gBlurKernel);
        applySeparableGaussianBlur(v_new, flags, gBlurKernel);
        v_new.multConst(2.0);
        v_new.mult(invA);
        v.mult(invA);
        v.sub(v_new);
    }

//! Precompute Q, a reused quantity in the PD iterations
//! Q = 2*G*G*(velT-velC)-sigma*velC
    void precomputeQ(MACGrid &Q, const FlagGrid &flags, const MACGrid &velT_region, const MACGrid &velC, const Matrix &gBlurKernel, const Real sigma) {
        Q.copyFrom(velT_region);
        Q.sub(velC);
        applySeparableGaussianBlur(Q, flags, gBlurKernel);
        applySeparableGaussianBlur(Q, flags, gBlurKernel);
        Q.multConst(2.0);
        Q.addScaled(velC, -sigma);
    }

//! Precompute inverse(A), a reused quantity in the PD iterations
//! A = 2*S^2 + p*I, invA = elementwise 1/A
    void precomputeInvA(MACGrid &invA, const Grid<Real> &weight, const Real sigma) {
        FOR_IJK(invA) {
                    Real val = 2 * weight(i, j, k)*weight(i, j, k) + sigma;
                    if (val<0.01) val = 0.01;
                    Real invVal = 1.0 / val;
                    invA(i, j, k).x = invVal;
                    invA(i, j, k).y = invVal;
                    invA(i, j, k).z = invVal;
                }
    }

//! proximal operator of f , guiding
    void prox_f(MACGrid& v, const FlagGrid &flags, const MACGrid& Q, const MACGrid& velC, const Real sigma, const MACGrid& invA) {
        v.multConst(sigma);
        v.add(Q);
        applyApproxInvM(v, flags, invA);
        v.add(velC);
    }

// *****************************************************************************

// re-uses main pressure solve from pressure.cpp
    void solvePressure(
            MACGrid& vel, Grid<Real>& pressure, const FlagGrid& flags, Real cgAccuracy = 1e-3,
            const Grid<Real>* phi = 0,
            const Grid<Real>* perCellCorr = 0,
            const MACGrid* fractions = 0,
            Real gfClamp = 1e-04,
            Real cgMaxIterFac = 1.5,
            bool precondition = true,
            int preconditioner = 1,
            bool enforceCompatibility = false,
            bool useL2Norm = false,
            bool zeroPressureFixing = false,
            const Grid<Real> *curv = NULL,
            const Real surfTens = 0.0,
            Grid<Real>* retRhs = NULL );

//! Main function for fluid guiding , includes "regular" pressure solve
    






void PD_fluid_guiding(MACGrid& vel, MACGrid& velT, Grid<Real>& pressure, FlagGrid& flags, Grid<Real>& weight, int blurRadius = 5, Real theta = 1.0, Real tau = 1.0, Real sigma = 1.0, Real epsRel = 1e-3, Real epsAbs = 1e-3, int maxIters = 200, Grid<Real>* phi = 0, Grid<Real>* perCellCorr = 0, MACGrid* fractions = 0, Real gfClamp = 1e-04, Real cgMaxIterFac = 1.5, Real cgAccuracy = 1e-3, int preconditioner = 1, bool zeroPressureFixing = false) {
        FluidSolver* parent = vel.getParent();

        // initialize dual/slack variables
        MACGrid velC = MACGrid(parent); velC.copyFrom(vel);
        MACGrid x = MACGrid(parent);
        MACGrid y = MACGrid(parent);
        MACGrid z = MACGrid(parent);
        MACGrid x0 = MACGrid(parent);
        MACGrid z0 = MACGrid(parent);

        // precomputation
        ADMM_precompute_Separable(blurRadius);
        MACGrid Q = MACGrid(parent);
        precomputeQ(Q, flags, velT, velC, gBlurKernel, sigma);
        MACGrid invA = MACGrid(parent);
        precomputeInvA(invA, weight, sigma);

        // loop
        int iter = 0;
        for (iter = 0; iter < maxIters; iter++) {
            // x-update
            x0.copyFrom(x);
            x.multConst(1.0 / sigma);
            x.add(y);
            prox_f(x, flags, Q, velC, sigma, invA);
            x.multConst(-sigma); x.addScaled(y, sigma); x.add(x0);

            // z-update
            z0.copyFrom(z);
            z.addScaled(x, -tau);
            Real cgAccuracyAdaptive = cgAccuracy;

            solvePressure (z, pressure, flags, cgAccuracyAdaptive, phi, perCellCorr, fractions, gfClamp,
                           cgMaxIterFac, true, preconditioner, false, false, zeroPressureFixing );

            // y-update
            y.copyFrom(z);
            y.sub(z0);
            y.multConst(theta);
            y.add(z);

            // stopping criterion
            bool stop = (iter > 0 && getRNorm(z, z0) < getEpsDual(epsAbs, epsRel, z));

            if (stop || (iter == maxIters - 1)) break;
        }

        // vel_new = z
        vel.copyFrom(z);

        debMsg("PD_fluid_guiding iterations:" << iter, 1);
    } static PyObject* _W_3 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "PD_fluid_guiding" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; MACGrid& vel = *_args.getPtr<MACGrid >("vel",0,&_lock); MACGrid& velT = *_args.getPtr<MACGrid >("velT",1,&_lock); Grid<Real>& pressure = *_args.getPtr<Grid<Real> >("pressure",2,&_lock); FlagGrid& flags = *_args.getPtr<FlagGrid >("flags",3,&_lock); Grid<Real>& weight = *_args.getPtr<Grid<Real> >("weight",4,&_lock); int blurRadius = _args.getOpt<int >("blurRadius",5,5,&_lock); Real theta = _args.getOpt<Real >("theta",6,1.0,&_lock); Real tau = _args.getOpt<Real >("tau",7,1.0,&_lock); Real sigma = _args.getOpt<Real >("sigma",8,1.0,&_lock); Real epsRel = _args.getOpt<Real >("epsRel",9,1e-3,&_lock); Real epsAbs = _args.getOpt<Real >("epsAbs",10,1e-3,&_lock); int maxIters = _args.getOpt<int >("maxIters",11,200,&_lock); Grid<Real>* phi = _args.getPtrOpt<Grid<Real> >("phi",12,0,&_lock); Grid<Real>* perCellCorr = _args.getPtrOpt<Grid<Real> >("perCellCorr",13,0,&_lock); MACGrid* fractions = _args.getPtrOpt<MACGrid >("fractions",14,0,&_lock); Real gfClamp = _args.getOpt<Real >("gfClamp",15,1e-04,&_lock); Real cgMaxIterFac = _args.getOpt<Real >("cgMaxIterFac",16,1.5,&_lock); Real cgAccuracy = _args.getOpt<Real >("cgAccuracy",17,1e-3,&_lock); int preconditioner = _args.getOpt<int >("preconditioner",18,1,&_lock); bool zeroPressureFixing = _args.getOpt<bool >("zeroPressureFixing",19,false,&_lock);   _retval = getPyNone(); PD_fluid_guiding(vel,velT,pressure,flags,weight,blurRadius,theta,tau,sigma,epsRel,epsAbs,maxIters,phi,perCellCorr,fractions,gfClamp,cgMaxIterFac,cgAccuracy,preconditioner,zeroPressureFixing);  _args.check(); } pbFinalizePlugin(parent,"PD_fluid_guiding", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("PD_fluid_guiding",e.what()); return 0; } } static const Pb::Register _RP_PD_fluid_guiding ("","PD_fluid_guiding",_W_3);  extern "C" { void PbRegister_PD_fluid_guiding() { KEEP_UNUSED(_RP_PD_fluid_guiding); } } 

// ############################################################ ZF
    void getfftVectors(MACGrid &vel, vector<double> &valuesX, vector<double> &valuesY, vector<double> &valuesZ, Vec3i res, bool lowSim, Vec3i lowres) {


        FOR_IJK(vel) {
//            for(int j=0; j<lowres; j++ ) {
//                for (int i = 0; i < lowres; i++) {
                    int idx = i + res.x * (j + res.y * k);

                    int idx_pad_x = (i+1) + (res.x+2) * (j + (res.y+2)* k);
                    int idx_pad_y = i + (res.x+2) * ((j+1) + (res.y+2) * k);
                    int idx_pad_z = i + (res.x+2)* (j + (res.y+2) * (k+1));



                    if (lowSim) {
                        int idx_lowres = i + lowres.x * (j + lowres.y * k);

                        int idx_pad_lowres_x = (i+1) + (lowres.x+2) * (j + (lowres.y+2)* k);
                        int idx_pad_lowres_y = i + (lowres.x+2) * ((j+1) + (lowres.y+2) * k);
                        int idx_pad_lowres_z = i + (lowres.x+2)* (j + (lowres.y+2) * (k+1));


                        if (i < lowres.x && j < lowres.y && k < lowres.z) {
                            valuesX[idx_pad_lowres_x] = vel(idx).x;
                            valuesY[idx_pad_lowres_y] = vel(idx).y;
                            if(vel.is3D())
                                valuesZ[idx_pad_lowres_z] = vel(idx).z;

                        }
                    }
                    else{
                        valuesX[idx_pad_x] = vel(idx).x;
                        valuesY[idx_pad_y] = vel(idx).y;
                        if(vel.is3D())
                            valuesZ[idx_pad_y] = vel(idx).z;

                    }
                }

        std::cout << " end getfft"<<endl;



    }

    void setfftToMACG(MACGrid &vel, vector<double> &valuesX, vector<double> &valuesY, vector<double> &valuesZ, Vec3i res) {



//    for(int idx=0; idx<(res*res); idx++ ) {
        FOR_IJK(vel){
                    int idx = i + res.x*(j +res.y*k);

                    int idx_pad_x = (i+1) + (res.x+2) * (j + (res.y+2)* k);
                    int idx_pad_y = i + (res.x+2) * ((j+1) + (res.y+2) * k);
                    int idx_pad_z = i + (res.x+2)* (j + (res.y+2) * (k+1));

                    vel(idx).x = valuesX[idx_pad_x];
                    vel(idx).y = valuesY[idx_pad_y];
                    if(vel.is3D()) vel(idx).z = valuesZ[idx_pad_z];

                }

        std::cout << " end setfft"<<endl;

    }

    void fft(vector<double> &valuesX, vector<double> &valuesY, vector<double> &valuesZ, Vec3i res, bool threeD)
    {

        double *ptrx =  &valuesX[0];
        double *ptry =  &valuesY[0];
        double *ptrz =  &valuesZ[0];
        fftw_plan fftplan_x, fftplan_y, fftplan_z;



//    fftplan_x = fftw_plan_r2r_2d(res+1, res, ptrx, ptrx, FFTW_RODFT00, FFTW_REDFT10, FFTW_ESTIMATE);
//    fftplan_y = fftw_plan_r2r_2d(res, res+1, ptry, ptry, FFTW_REDFT10, FFTW_RODFT00, FFTW_ESTIMATE);
        if(!threeD) {
            std::cout  << " valuesX "<< valuesX.size() << " valuesY " << valuesY.size()  << " valuesZ " << valuesZ.size()  << " res: " << res << endl;
            fftplan_x = fftw_plan_r2r_2d(res.x+2, res.y, ptrx, ptrx, FFTW_REDFT10, FFTW_REDFT00, FFTW_MEASURE);
            fftplan_y = fftw_plan_r2r_2d(res.x, res.y+2, ptry, ptry, FFTW_REDFT00, FFTW_REDFT10, FFTW_MEASURE);

        }
        else{
            fftplan_x = fftw_plan_r2r_3d(res.x+2, res.y, res.z, ptrx, ptrx, FFTW_REDFT10, FFTW_REDFT00, FFTW_REDFT00, FFTW_ESTIMATE);
            fftplan_y = fftw_plan_r2r_3d(res.x, res.y+2, res.z, ptry, ptry, FFTW_REDFT00, FFTW_REDFT10, FFTW_REDFT00, FFTW_ESTIMATE);
            fftplan_z = fftw_plan_r2r_3d(res.x, res.y, res.z+2, ptrz, ptrz, FFTW_REDFT00, FFTW_REDFT00, FFTW_REDFT10, FFTW_ESTIMATE);
        }


        fftw_execute(fftplan_x);
        fftw_execute(fftplan_y);
        if(threeD) fftw_execute(fftplan_z);

        int total_size =0;
//        if(threeD)
//            total_size = 8*res.x*res.y*res.z;
//        else
//            total_size = 4*res.x*res.y;

//        if(threeD)
//            total_size = 8*(res.x+2)*(res.y+2)*(res.z+2);
//        else
//            total_size = 4*(res.x+2)*(res.y+2);


//        for(int i=0; i< valuesX.size(); i++){
//
//            valuesX[i] /= total_size;
//            valuesY[i] /= total_size;
//            if(threeD) valuesZ[i] /= total_size;
//
//
//        }


        for(int i=0; i< valuesX.size(); i++) {
            valuesX[i] /= 4 * (res.x + 2) * res.y ;
            if(threeD) valuesX[i] /= 8 * (res.x + 2) * res.y * res.z;
        }

            for(int i=0; i< valuesY.size(); i++){
                valuesY[i] /= 4 * res.x  * (res.y +2);
                if(threeD) valuesX[i] /= 8 * res.x  * (res.y +2) * res.z;
            }
            if(threeD)
                for(int i=0; i< valuesZ.size(); i++)
                    valuesZ[i] /= 8 * res.x  * res.y  * (res.z+2);



            fftw_destroy_plan(fftplan_x);
            fftw_destroy_plan(fftplan_y);
            if(threeD) fftw_destroy_plan(fftplan_z);

        std::cout << " end fft"<<endl;


    }




        void ideal_filtering_smoke_guiding(MACGrid& vel_lowres,MACGrid& vel_highres, Vec3i lowres, Vec3i highres,double cutoff){


//            vector<double> valuesX_lowres(lowres.x*lowres.y*lowres.z);
//            vector<double> valuesY_lowres(lowres.x*lowres.y*lowres.z);
//            vector<double> valuesZ_lowres(lowres.x*lowres.y*lowres.z);

            vector<double> valuesX_lowres((lowres.x+2)*lowres.y*lowres.z);
            vector<double> valuesY_lowres(lowres.x*(lowres.y+2)*lowres.z);
            vector<double> valuesZ_lowres(lowres.x*lowres.y*(lowres.z+2));

//            vector<double> valuesX_highres(highres.x*highres.y*highres.z);
//            vector<double> valuesY_highres(highres.x*highres.y*highres.z);
//            vector<double> valuesZ_highres(highres.x*highres.y*highres.z);


            vector<double> valuesX_highres((highres.x+2)*highres.y*highres.z);
            vector<double> valuesY_highres(highres.x*(highres.y+2)*highres.z);
            vector<double> valuesZ_highres(highres.x*highres.y*(highres.z+2));


            getfftVectors(vel_lowres, valuesX_lowres, valuesY_lowres, valuesZ_lowres, highres, true, lowres );
            getfftVectors(vel_highres, valuesX_highres, valuesY_highres, valuesZ_highres, highres, false, lowres );


            double *ptrx = &valuesX_highres[0];
            double *ptry = &valuesY_highres[0];
            double *ptrz = &valuesZ_highres[0];
            fftw_plan ifftplan_x, ifftplan_y, ifftplan_z  ;

            if(vel_highres.is3D()) {
                ifftplan_x = fftw_plan_r2r_3d(highres.x, highres.y, highres.z, ptrx, ptrx, FFTW_REDFT01, FFTW_REDFT00,
                                              FFTW_REDFT00, FFTW_ESTIMATE);
                ifftplan_y = fftw_plan_r2r_3d(highres.x, highres.y, highres.z, ptry, ptry, FFTW_REDFT00, FFTW_REDFT01,
                                              FFTW_REDFT00, FFTW_ESTIMATE);
                ifftplan_z = fftw_plan_r2r_3d(highres.x, highres.y, highres.z, ptrz, ptrz, FFTW_REDFT00, FFTW_REDFT00,
                                              FFTW_REDFT01, FFTW_ESTIMATE);
            } else{
                ifftplan_x = fftw_plan_r2r_2d(highres.x+2, highres.y, ptrx, ptrx, FFTW_REDFT00, FFTW_REDFT01, FFTW_ESTIMATE);
                ifftplan_y = fftw_plan_r2r_2d(highres.x, highres.y+2, ptry, ptry, FFTW_REDFT01, FFTW_REDFT00, FFTW_ESTIMATE);

            }


            std::cout << "lowres" <<endl;
            fft(valuesX_lowres, valuesY_lowres, valuesZ_lowres, lowres,vel_highres.is3D());

            std::cout << "highres" <<endl;
            fft(valuesX_highres, valuesY_highres, valuesZ_highres, highres, vel_highres.is3D());




//            FOR_IJK(vel_highres) {
//
//                        int idx_lowres = 0;
//                        int idx_highres = 0;
//                        int dist =0;
//
//                        if(vel_highres.is3D()) {
//
//                            idx_lowres = i + lowres.x * (j + lowres.y * k);
//                            idx_highres = i + highres.x * (j + highres.y * k);
//                            dist = pow(i, 2) + pow(j, 2) + pow(k, 2);
//
//                        } else{
//                            idx_lowres = i + lowres.x * j ;
//                            idx_highres = i + highres.x * j ;
//                            dist = pow(i, 2) + pow(j, 2);
//
//                        }

//                        if (dist < pow(cutoff, 2)) {
//
//                            valuesX_highres[idx_highres] = valuesX_lowres[idx_lowres] * (highres.x / lowres.x);
//                            valuesY_highres[idx_highres] = valuesY_lowres[idx_lowres] * (highres.y / lowres.y);
//                            if(vel_highres.is3D()) valuesZ_highres[idx_highres] = valuesZ_lowres[idx_lowres] * (highres.z / lowres.z);
//
//                        }
//                    }

            fftw_execute(ifftplan_x);
            fftw_execute(ifftplan_y);
            if(vel_highres.is3D()) fftw_execute(ifftplan_z);

            setfftToMACG(vel_highres,valuesX_highres, valuesY_highres, valuesZ_highres, highres);

            fftw_destroy_plan(ifftplan_x);
            fftw_destroy_plan(ifftplan_y);
            if(vel_highres.is3D()) fftw_destroy_plan(ifftplan_z);

        std::cout << " end guiding"<<endl;

        } static PyObject* _W_4 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "ideal_filtering_smoke_guiding" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; MACGrid& vel_lowres = *_args.getPtr<MACGrid >("vel_lowres",0,&_lock); MACGrid& vel_highres = *_args.getPtr<MACGrid >("vel_highres",1,&_lock); Vec3i lowres = _args.get<Vec3i >("lowres",2,&_lock); Vec3i highres = _args.get<Vec3i >("highres",3,&_lock); double cutoff = _args.get<double >("cutoff",4,&_lock);   _retval = getPyNone(); ideal_filtering_smoke_guiding(vel_lowres,vel_highres,lowres,highres,cutoff);  _args.check(); } pbFinalizePlugin(parent,"ideal_filtering_smoke_guiding", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("ideal_filtering_smoke_guiding",e.what()); return 0; } } static const Pb::Register _RP_ideal_filtering_smoke_guiding ("","ideal_filtering_smoke_guiding",_W_4);  extern "C" { void PbRegister_ideal_filtering_smoke_guiding() { KEEP_UNUSED(_RP_ideal_filtering_smoke_guiding); } } 


//######################################## writing and reading file

        void writeEachDirection(string fileName,MACGrid& vel, Vec3i res, int dir) {

            FILE *file;
            string fileNameOut = fileName;
            const char *fileN = fileNameOut.c_str();
            file = fopen(fileN, "w");

            FOR_IJK(vel){

//            for (int j = 0; j < res; j++) {
//                for (int i = 0; i < res; i++) {



                        int idx = i + res.x * (j + res.y * k);
//                    std::cout << "k " << k << "," << j << "," << i << "|"<< idx<<endl;


                        if (idx == 0) {
                            if (dir == 0) fprintf(file, " %.4f ", vel(idx).x);
                            if (dir == 1) fprintf(file, " %.4f ", vel(idx).y);
                            if (dir == 2) fprintf(file, " %.4f ", vel(idx).z);
                        } else {
                            if (dir == 0) fprintf(file, " ,%.4f ", vel(idx).x);
                            if (dir == 1) fprintf(file, " ,%.4f ", vel(idx).y);
                            if (dir == 2) fprintf(file, " ,%.4f ", vel(idx).z);
                        }

//                }
//            }

                    }

            fclose (file);

        }

        void writeEachDirectionBinary(string fileName,MACGrid& vel, Vec3i res, int dir) {

            FILE *file;
            string fileNameOut = fileName;
            const char *fileN = fileNameOut.c_str();
            file = fopen(fileN, "wb");

            FOR_IJK(vel) {

                        int idx = i + res.x * (j + res.y * k);

                        if (dir == 0) fwrite(&vel(idx).x, sizeof(Real), 1, file);
                        if (dir == 1) fwrite(&vel(idx).y, sizeof(Real), 1, file);
                        if (dir == 2) fwrite(&vel(idx).z, sizeof(Real), 1, file);

                    }

            fclose (file);

        }

        void writeOutMACGrid(string folderName, MACGrid& vel,Vec3i res, int frameNum, bool binary=false){

            string ext = binary ? ".bin" : ".txt";

            string fileNameOutX = folderName + "vx/"+ std::to_string(frameNum) + ext;
            string fileNameOutY = folderName + "vy/"+ std::to_string(frameNum) + ext;
            string fileNameOutZ = folderName + "vz/"+ std::to_string(frameNum) + ext;
            std::cout << "writing file: " << fileNameOutX << endl;

            if (binary) {
                writeEachDirectionBinary(fileNameOutX,vel,res, 0);
                writeEachDirectionBinary(fileNameOutY,vel,res, 1);
                if(vel.is3D()) {
                    writeEachDirectionBinary(fileNameOutZ, vel, res, 2);
                }
            } else {
                writeEachDirection(fileNameOutX,vel,res, 0);
                writeEachDirection(fileNameOutY,vel,res, 1);
                if(vel.is3D()) {
                    writeEachDirection(fileNameOutZ, vel, res, 2);
                }
            }

        } static PyObject* _W_5 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "writeOutMACGrid" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; string folderName = _args.get<string >("folderName",0,&_lock); MACGrid& vel = *_args.getPtr<MACGrid >("vel",1,&_lock); Vec3i res = _args.get<Vec3i >("res",2,&_lock); int frameNum = _args.get<int >("frameNum",3,&_lock); bool binary = _args.getOpt<bool >("binary",4,false,&_lock);   _retval = getPyNone(); writeOutMACGrid(folderName,vel,res,frameNum,binary);  _args.check(); } pbFinalizePlugin(parent,"writeOutMACGrid", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("writeOutMACGrid",e.what()); return 0; } } static const Pb::Register _RP_writeOutMACGrid ("","writeOutMACGrid",_W_5);  extern "C" { void PbRegister_writeOutMACGrid() { KEEP_UNUSED(_RP_writeOutMACGrid); } } 

        void readEachDirection(string fileName,MACGrid& vel,Vec3i res, int dir, Vec3i lowres){

            std::cout << "reading file: " << fileName << endl;

            int size = lowres.x*lowres.y*lowres.z;
            float velArray[size];
            string fileNameIn = fileName;

            string line;

            std::ifstream file(fileNameIn);
            while(getline(file,line))
            {
                std::stringstream   linestream(line);
                std::string         valueStr;
                float               value;
                int i=0;

                while(getline(linestream,valueStr,','))
                {
                    value = std::stof(valueStr);
                    velArray[i]= value;
                    i++;
                }

            }
//    vel.printGrid(-1,false,1);

            FOR_IJK(vel){
//        for(int j=0; j<res; j++){
//            for(int i=0; i<res; i++) {
//          std:: cout << "vel indx: (" <<i<<","<<j<<","<<k<<")->"<<indx <<endl;
                        int idx = i + res.x * (j +res.y*k);
                        int idx_lowres = i + lowres.x * (j +lowres.y*k);

                        if(i<lowres.x && j<lowres.y && k<lowres.z) {
                            if (dir == 0) vel(idx).x = velArray[idx_lowres];
                            if (dir == 1) vel(idx).y = velArray[idx_lowres];
                            if (dir == 2) vel(idx).z = velArray[idx_lowres];
                        }
                        else{
                            if (dir == 0) vel(idx).x = 0.;
                            if (dir == 1) vel(idx).y = 0.;
                            if (dir == 2) vel(idx).z = 0.;
                        }
//       }             if(dir == 2) vel(i,j,k).z = velArray[indx];
//            }
                    }


        }

        void readEachDirectionBinary(string fileName,MACGrid& vel,Vec3i res, int dir, Vec3i lowres){

            std::cout << "reading file: " << fileName << endl;

            int size = lowres.x * lowres.y * lowres.z;
            Real *velArray = new Real[size];
            string fileNameIn = fileName;

            FILE *file = fopen(fileName.c_str(), "rb");
            fread(velArray, sizeof(Real), size, file);
            fclose(file);

            FOR_IJK(vel){
                        int idx = i + res.x * (j +res.y*k);
                        int idx_lowres = i + lowres.x * (j +lowres.y*k);

                        if(i<lowres.x && j<lowres.y && k<lowres.z) {
                            if (dir == 0) vel(idx).x = velArray[idx_lowres];
                            if (dir == 1) vel(idx).y = velArray[idx_lowres];
                            if (dir == 2) vel(idx).z = velArray[idx_lowres];
                        }
                        else{
                            if (dir == 0) vel(idx).x = 0.;
                            if (dir == 1) vel(idx).y = 0.;
                            if (dir == 2) vel(idx).z = 0.;
                        }
                    }

            delete[] velArray;


        }


        void readInMACGrid(string folderName,MACGrid& vel, int frameNum, Vec3i res, Vec3i lowres, bool binary){ //, MACGrid *vel, int sizeGrid, int frameNum, int dir
            string ext = binary ? ".bin" : ".txt";
            string fileNameX = folderName + "vx/"+ std::to_string(frameNum) + ext;
            string fileNameY = folderName + "vy/"+ std::to_string(frameNum) + ext;
            string fileNameZ = folderName + "vz/"+ std::to_string(frameNum) + ext;

            if (binary) {
                readEachDirectionBinary(fileNameX, vel,res, 0, lowres);
                readEachDirectionBinary(fileNameY, vel,res, 1, lowres);
                if(vel.is3D()) readEachDirectionBinary(fileNameZ, vel,res, 2, lowres);
            } else {
                readEachDirection(fileNameX, vel,res, 0, lowres);
                readEachDirection(fileNameY, vel,res, 1, lowres);
                if(vel.is3D()) readEachDirection(fileNameZ, vel,res, 2, lowres);
            }





        } static PyObject* _W_6 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "readInMACGrid" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; string folderName = _args.get<string >("folderName",0,&_lock); MACGrid& vel = *_args.getPtr<MACGrid >("vel",1,&_lock); int frameNum = _args.get<int >("frameNum",2,&_lock); Vec3i res = _args.get<Vec3i >("res",3,&_lock); Vec3i lowres = _args.get<Vec3i >("lowres",4,&_lock); bool binary = _args.get<bool >("binary",5,&_lock);   _retval = getPyNone(); readInMACGrid(folderName,vel,frameNum,res,lowres,binary);  _args.check(); } pbFinalizePlugin(parent,"readInMACGrid", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("readInMACGrid",e.what()); return 0; } } static const Pb::Register _RP_readInMACGrid ("","readInMACGrid",_W_6);  extern "C" { void PbRegister_readInMACGrid() { KEEP_UNUSED(_RP_readInMACGrid); } } 


// function to get the divergence and write it out
        void writeRealGrid(string folderName, Grid< Real >& gridR, Vec3i res, int frameNum ) {

            string fileName = folderName + std::to_string(frameNum) + ".txt";

            FILE *file;
            string fileNameOut = fileName;
            const char *fileN = fileNameOut.c_str();
            file = fopen(fileN, "w");

            FOR_IJK(gridR){


                        int idx = i + res.x * (j + res.y * k);

                        if (idx == 0) fprintf(file, " %.4f ", gridR(idx));
                        else  fprintf(file, " ,%.4f ", gridR(idx));



                    }

            fclose (file);

        } static PyObject* _W_7 (PyObject* _self, PyObject* _linargs, PyObject* _kwds) { try { PbArgs _args(_linargs, _kwds); FluidSolver *parent = _args.obtainParent(); bool noTiming = _args.getOpt<bool>("notiming", -1, 0); pbPreparePlugin(parent, "writeRealGrid" , !noTiming ); PyObject *_retval = 0; { ArgLocker _lock; string folderName = _args.get<string >("folderName",0,&_lock); Grid< Real >& gridR = *_args.getPtr<Grid< Real > >("gridR",1,&_lock); Vec3i res = _args.get<Vec3i >("res",2,&_lock); int frameNum = _args.get<int >("frameNum",3,&_lock);   _retval = getPyNone(); writeRealGrid(folderName,gridR,res,frameNum);  _args.check(); } pbFinalizePlugin(parent,"writeRealGrid", !noTiming ); return _retval; } catch(std::exception& e) { pbSetError("writeRealGrid",e.what()); return 0; } } static const Pb::Register _RP_writeRealGrid ("","writeRealGrid",_W_7);  extern "C" { void PbRegister_writeRealGrid() { KEEP_UNUSED(_RP_writeRealGrid); } } 


//    PYTHON() void divRealGrid(Grid<Real>& div, const MACGrid& grid) {
//
//        DivergenceOpMAC(div,grid);
////        Vec3i idx =  grid.getSize();
////        FOR_IJK(grid) {
////                    if(i<idx[0]-1 && j<idx[1]-1) {
////                        Vec3 del = Vec3(grid(i + 1, j, k).x, grid(i, j + 1, k).y, 0.) - grid(i, j, k);
////                        if (grid.is3D()) {
////                            if(k<idx[2]-1) del[2] += grid(i, j, k + 1).z;
////                        }
////                        else del[2] = 0.;
////                        div(i, j, k) = del.x + del.y + del.z;
////                    }
////                }
//    }



    } // end namespace



