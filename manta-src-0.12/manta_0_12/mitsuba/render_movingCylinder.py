#!/usr/bin/python

### Usage: ./render.py <sceneXml> <simDir> <frameStart> <frameEnd> <frameStep>
### e.g. ./render.py simpleplume simpleplume 0 100 10

### Renders simulation data in <simDir> using scene <sceneXml>

import os
import subprocess
import sys


#mitsuba_win_dim_s = [-2,-4,-2]
#mitsuba_win_dim_e = [2, 4, 2]

#mitsuba_win_w =[e - s for e,s in zip(mitsuba_win_dim_e ,  mitsuba_win_dim_s)] 

sceneXml = sys.argv[1]
#simDir_obst = sys.argv[2]
simDir = sys.argv[2]
sim_dim_x = float(sys.argv[3])
sim_dim_y = float(sys.argv[4])
sim_dim_z = float(sys.argv[5])

win_dim_x =  int(sys.argv[6])
win_dim_y =  int(sys.argv[7])
win_dim_z =  int(sys.argv[8])

start = int(sys.argv[9])
end = int(sys.argv[10])
step = int(sys.argv[11])


win_dim_sx =  -1*win_dim_x
win_dim_sy =  -1*win_dim_y
win_dim_sz =  -1*win_dim_z
mitsuba_win_dim_s = [-1*win_dim_x,-1*win_dim_y,-1*win_dim_z]
mitsuba_win_dim_e = [win_dim_x, win_dim_y, win_dim_z]

mitsuba_win_w =[e - s for e,s in zip(mitsuba_win_dim_e ,  mitsuba_win_dim_s)]
heightw = 1080
widthw = int(heightw *(float(win_dim_x)/win_dim_y))

obsSizefile = "%sobstSize.txt" % simDir
fob = open(obsSizefile, 'r')

for line in fob:
    obs_size = float(line)*(mitsuba_win_w[0]/sim_dim_x)

for i in range(start,end,step):
    volfile = "%sdensity_%04d.vol" % (simDir, i)
    imgfile = "%srender_%04d.png" % (simDir, i)
    obstfile = "%sobstPos_%04d.txt" % (simDir, i)
    f = open(obstfile, 'r')
    for line in f:
        obsp= line.split(" ")
        p0_x = mitsuba_win_dim_s[0] + float(obsp[0])*(mitsuba_win_w[0]/sim_dim_x)
        p0_y = mitsuba_win_dim_s[1] + float(obsp[1])*(mitsuba_win_w[1]/sim_dim_y)
        p0_z = mitsuba_win_dim_s[2] + float(obsp[2])*(mitsuba_win_w[2]/sim_dim_z)

        p1_x = mitsuba_win_dim_s[0] + float(obsp[3])*(mitsuba_win_w[0]/sim_dim_x)
        p1_y = mitsuba_win_dim_s[1] + float(obsp[4])*(mitsuba_win_w[1]/sim_dim_y)
        p1_z = mitsuba_win_dim_s[2] + float(obsp[5])*(mitsuba_win_w[2]/sim_dim_z)
	
	print "p: ", p0_x, p0_y,p0_z, p1_x,p1_y, p1_z
	subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-D", "obstSize=%5.2f" % obs_size, "-D", "p0_x=%5.2f" % p0_x,  "-D", "p0_y=%5.2f" % p0_y, "-D", "p0_z=%5.2f" % p0_z, "-D", "p1_x=%5.2f" % p1_x, "-D", "p1_y=%5.2f" % p1_y, "-D", "p1_z=%5.2f" % p1_z,"-D","win_sx=%d"%win_dim_sx,"-D","win_sy=%d"% win_dim_sy, "-D","win_sz=%d"%win_dim_sz,"-D","win_ex=%d"% win_dim_x,"-D","win_ey=%d"% win_dim_y, "-D","win_ez=%d"% win_dim_z,"-D","heightw=%d"%heightw,"-D","widthw=%d"%widthw , "-o", imgfile, "-r", "5", sceneXml])

    print "Finished rendering frame: ", i
