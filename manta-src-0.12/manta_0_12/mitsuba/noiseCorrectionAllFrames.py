import struct
import sys
import os


import struct
import sys

args = len(sys.argv)
if args < 3:
	print "incorrect number of arguments"
	sys.exit()

pycode = sys.argv[1]
voldir = sys.argv[2]
num_frames = int(sys.argv[3])
# frames = int(num_frames)

print(pycode ,voldir, num_frames)

new_dir = voldir +"/noise_corrected/"
if not os.path.exists(new_dir):
    os.makedirs(new_dir)

for i in range(num_frames):
    # os.pause(10)
    print("correcting vol ",i)
    # os.system("python "+pycode +" "+ ' %s/density_%04d.vol' % (voldir, i) + " "+ '%snoise_corrected/density_%04d.vol' % (voldir, i) +" "+str(0.01) +" "+str(0.9) )
    os.system('python '+pycode +' '+ '%s/density_%04d.vol' % (voldir, i) + ' '+ '%s/density_%04d.vol' % (new_dir, i))




# python noiseCorrection.py output/plume/lowres_50X100X50/density_0124.vol output/plume/lowres_50X100X50/density_0124_corr.vol 1e-9 1