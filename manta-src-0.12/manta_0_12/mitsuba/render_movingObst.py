#!/usr/bin/python

### Usage: ./render.py <sceneXml> <simDir> <frameStart> <frameEnd> <frameStep>
### e.g. ./render.py simpleplume simpleplume 0 100 10

### Renders simulation data in <simDir> using scene <sceneXml>

import os
import subprocess
import sys


mitsuba_win_dim_s = [-2,-4,-2]
mitsuba_win_dim_e = [2, 4, 2]

mitsuba_win_w =[e - s for e,s in zip(mitsuba_win_dim_e ,  mitsuba_win_dim_s)] 

sceneXml = sys.argv[1]
simDir_obst = sys.argv[2]
simDir = sys.argv[3]
sim_dim_x = float(sys.argv[4])
sim_dim_y = float(sys.argv[5])
sim_dim_z = float(sys.argv[6])
start = int(sys.argv[7])
end = int(sys.argv[8])
step = int(sys.argv[9])
obsSizefile = "%sobstSize.txt" % simDir_obst
fob = open(obsSizefile, 'r')

for line in fob:
    obs_size = float(line)*(mitsuba_win_w[0]/sim_dim_x)

for i in range(start,end,step):
    volfile = "%s/density_%04d.vol" % (simDir, i)
    imgfile = "%s/render_%04d.png" % (simDir, i)
    obstfile = "%s/obstPos_%04d.txt" % (simDir_obst, i)
    f = open(obstfile, 'r')
    for line in f:
        obsp= line.split(" ")
        obsp_x = mitsuba_win_dim_s[0] + float(obsp[0])*(mitsuba_win_w[0]/sim_dim_x)
        obsp_y = mitsuba_win_dim_s[1] + float(obsp[1])*(mitsuba_win_w[1]/sim_dim_y)
        obsp_z = mitsuba_win_dim_s[2] + float(obsp[2])*(mitsuba_win_w[2]/sim_dim_z)

    print("size: ", obs_size, "   obsp: ", obsp_x, " ", obsp_y, " ", obsp_z)
    subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-D", "obstSize=%5.2f" % obs_size, "-D", "obstPos_x=%5.2f" % obsp_x,  "-D", "obstPos_y=%5.2f" % obsp_y, "-D", "obstPos_z=%5.2f" % obsp_z, "-o", imgfile, "-r", "5", sceneXml])
    print "Finished rendering frame: ", i
