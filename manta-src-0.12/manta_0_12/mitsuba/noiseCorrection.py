import struct
import sys

args = len(sys.argv)
if args < 3:
	print "incorrect number of arguments"
	sys.exit()

inputFile = sys.argv[1]
outputFile = sys.argv[2]

vmin = 0.07
vmax = 0.9

if args == 5:
    vmin = sys.argv[3]
    vmax = sys.argv[4]


with open(inputFile, "rb") as f:
    byteVOL = f.read(3)	# Bytes 1-3		ASCII "VOL"
    byteVer = f.read(1)	# Bytes 4		File format version number
    byteEncode = f.read(4)	# Bytes 5-8		Encoding identifier
    byteDim = f.read(12)	# Bytes 9-20	X, Y, Z dimension
    [dimX, dimY, dimZ] = struct.unpack("iii", byteDim)
    byteChan = f.read(4)	# Bytes 21-24	Number of channels
    nChan = struct.unpack("i", byteChan)[0]
    byteBox = f.read(24)	# Bytes 25-48	Bounding box
    [xmin, ymin, zmin, xmax, ymax, zmax] = struct.unpack("ffffff", byteBox)
    # print [xmin, ymin, zmin, xmax, ymax, zmax]
    size = dimX*dimY*dimZ*nChan
    data = []
    for i in range(0, size):
    	byte = f.read(4)
        d = struct.unpack("f", byte)[0]
        # TODO: change the value d such that it's in the range of [0, 1]
        if d < vmin:
            d = 0.0
        elif d > vmax:
            d = 1.0
        else:
            d = (d - vmin)/(vmax - vmin)
    	data.append(d)

with open(outputFile, 'wb') as f:
	f.write(byteVOL)
	f.write(byteVer)
	f.write(byteEncode)
	f.write(byteDim)
	f.write(byteChan)
	f.write(byteBox)
	for d in data:
		f.write(struct.pack("f", d))
