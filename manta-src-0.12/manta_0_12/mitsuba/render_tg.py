#!/usr/bin/python

### Usage: ./render.py <sceneXml> <simDir> <frameStart> <frameEnd> <frameStep>
### e.g. ./render.py simpleplume simpleplume 0 100 10

### Renders simulation data in <simDir> using scene <sceneXml>

########################

# to render first run
#./manta ../mitsuba/output_tempoGAN/uni2vol.py ../mitsuba/output_tempoGAN/3ddata_sim/sim_3006/ 5 256 1 0 (input tempoGAN highres)
#./manta ../mitsuba/output_tempoGAN/uni2vol.py ../mitsuba/output_tempoGAN/3ddata_sim/sim_3006/ 5 64 1 1 (input to tempoGAN lowres)
#./manta ../mitsuba/output_tempoGAN/uni2vol.py ../mitsuba/output_tempoGAN/3ddata_sim/sim_3006/ 5 256 0 0 (output of tempoGAN)

#then render such as
#./render_tg.py tg.xml output_tempoGAN/3ddata_sim/sim_3006 0 5 1 1 0(input tempoGAN highres)
#./render_tg.py tg.xml output_tempoGAN/3ddata_sim/sim_3006 0 5 1 1 1(input tempoGAN lowhres)
#./render_tg.py tg.xml output_tempoGAN/3ddata_sim/sim_3006 0 5 1 0 0(output tempoGAN)


#########################
import os
import subprocess
import sys

sceneXml = sys.argv[1]
simDir = sys.argv[2]
start = int(sys.argv[3])
end = int(sys.argv[4])
step = int(sys.argv[5])
inp = int(sys.argv[6])
lowres = int(sys.argv[7])


for i in range(start,end,step):


        if(inp):        
                if(lowres):
                        volfile = "%s/density_low_%4d.vol" % (simDir, i)
                        imgfile = "%s/render_low_%4d.png" % (simDir, i)
                        subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-o", imgfile, "-r", "5", sceneXml])
                else:
                        volfile = "%s/density_high_%4d.vol" % (simDir, i)
                        imgfile = "%s/render_high_%4d.png" % (simDir, i)
                        subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-o", imgfile, "-r", "5", sceneXml])
                
        else:
                volfile = "%s/source_%04d.vol" % (simDir, i)
                imgfile = "%s/render_%04d.png" % (simDir, i)
                subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-o", imgfile, "-r", "5", sceneXml])
