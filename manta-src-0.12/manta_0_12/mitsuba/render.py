#!/usr/bin/python

### Usage: ./render.py <sceneXml> <simDir> <frameStart> <frameEnd> <frameStep>
### e.g. ./render.py simpleplume simpleplume 0 100 10

### Renders simulation data in <simDir> using scene <sceneXml>

import os
import subprocess
import sys

sceneXml = sys.argv[1]
simDir = sys.argv[2]
start = int(sys.argv[3])
end = int(sys.argv[4])
step = int(sys.argv[5])

for i in range(start,end,step):
    volfile = "%sdensity_%04d.vol" % (simDir, i)
    imgfile = "%srender_%04d.png" % (simDir, i)
    subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-o", imgfile, "-r", "5", sceneXml])
