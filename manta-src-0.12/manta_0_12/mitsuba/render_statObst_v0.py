#!/usr/bin/python

### Usage: ./render.py <sceneXml> <simDir> <frameStart> <frameEnd> <frameStep>
### e.g. ./render.py simpleplume simpleplume 0 100 10

### Renders simulation data in <simDir> using scene <sceneXml>

import os
import subprocess
import sys




sceneXml = sys.argv[1]
simDir_obst = sys.argv[2]
simDir = sys.argv[3]
sim_dim_x = float(sys.argv[4])
sim_dim_y = float(sys.argv[5])
sim_dim_z = float(sys.argv[6])

win_dim_x =  int(sys.argv[7])
win_dim_y =  int(sys.argv[8])
win_dim_z =  int(sys.argv[9])

start = int(sys.argv[10])
end = int(sys.argv[11])
step = int(sys.argv[12])


#mitsuba_win_dim_s = [-4,-4,-2]
#mitsuba_win_dim_e = [4, 4, 2]
win_dim_sx =  -1*win_dim_x
win_dim_sy =  -1*win_dim_y
win_dim_sz =  -1*win_dim_z

mitsuba_win_dim_s = [-1*win_dim_x,-1*win_dim_y,-1*win_dim_z]
mitsuba_win_dim_e = [win_dim_x, win_dim_y, win_dim_z]

mitsuba_win_w =[e - s for e,s in zip(mitsuba_win_dim_e ,  mitsuba_win_dim_s)] 


heightw = 1080
widthw = int(heightw *(float(win_dim_x)/win_dim_y))

obsSizefile = "%sobstSize.txt" % simDir_obst
fob = open(obsSizefile, 'r')
for line in fob:
    obs_size = float(line)*(mitsuba_win_w[0]/sim_dim_x)

obstfile = "%sobstPos.txt" % simDir_obst
f = open(obstfile, 'r')
for line in f:
        obsp= line.split(" ")
        obsp_x = mitsuba_win_dim_s[0] + float(obsp[0])*(mitsuba_win_w[0]/sim_dim_x)
        obsp_y = mitsuba_win_dim_s[1] + float(obsp[1])*(mitsuba_win_w[1]/sim_dim_y)
        obsp_z = mitsuba_win_dim_s[2] + float(obsp[2])*(mitsuba_win_w[2]/sim_dim_z)

#print "mitsuba_win_dim_s", mitsuba_win_dim_s
#print "mitsuba_win_w", mitsuba_win_w
#print "obsp", obsp
#print "final obsp:", obsp_x,obsp_y,obsp_z
#print "obs_size:", obs_size

for i in range(start,end,step):
    volfile = "%s/density_%04d.vol" % (simDir, i)
    imgfile = "%s/render_%04d.png" % (simDir, i)
#    subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-o", imgfile, "-r", "5", sceneXml])
    subprocess.call(["mitsuba", "-D", "volfile=%s" % volfile, "-D", "obstSize=%5.2f" % obs_size, "-D", "obstPos_x=%5.2f" % obsp_x,  "-D", "obstPos_y=%5.2f" % obsp_y, "-D", "obstPos_z=%5.2f" % obsp_z, "-D","win_sx=%d"%win_dim_sx, "-D","win_sy=%d"% win_dim_sy , "-D","win_sz=%d"%win_dim_sz, "-D","win_ex=%d"%win_dim_x, "-D","win_ey=%d"% win_dim_y, "-D","win_ez=%d"% win_dim_z,"-D","heightw=%d"%heightw,"-D","widthw=%d"%widthw ,"-o", imgfile, "-r", "5", sceneXml])



print "Finished rendering frame: ", i
