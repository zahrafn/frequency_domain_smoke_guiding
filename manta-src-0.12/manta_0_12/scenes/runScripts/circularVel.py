import struct
import sys
import os

# new_dir = voldir +"/noise_corrected/"
# if not os.path.exists(new_dir):
#     os.makedirs(new_dir)

# os.system('python '+pycode +' '+ '%s/density_%04d.vol' % (voldir, i) + ' '+ '%s/density_%04d.vol' % (new_dir, i))
os.chdir("../../build")
print("=========================== our method Ideal guiding")
os.system("./manta ../scenes/circular_velocity.py 128 1 0.33333 180")
os.system("./manta ../scenes/circular_velocity.py 128 1 0.16666 180")
os.system("./manta ../scenes/circular_velocity.py 128 1 0.11111 180")
os.system("./manta ../scenes/circular_velocity.py 128 1 0.08333 180")

print("=========================== primal dual method")
os.system("./manta ../scenes/guiding_2d.py 128 1 2 180")
os.system("./manta ../scenes/guiding_2d.py 128 1 4 180")
os.system("./manta ../scenes/guiding_2d.py 128 1 6 180")
os.system("./manta ../scenes/guiding_2d.py 128 1 8 180")

# Note: chose nu = 1/(3/2 beta)



# python noiseCorrection.py output/plume/lowres_50X100X50/density_0124.vol output/plume/lowres_50X100X50/density_0124_corr.vol 1e-9 1
