import struct
import sys
import os

res_lowr_x = 64
res_lowr_y = 64
res_lowr_z = 32

win_sx = 4
win_sy = 4
win_sz = 2
num_frames = 180
scale = 3

print("================ LOW RES")

os.chdir("../../build")

os.system("./manta ../scenes/knot.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1")
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/knot/knot_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
dirct = "output_fall20/knot/knot_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/"
os.system("./render_statObst.py knot_closeup.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_fall20/knot/knot_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0")
output_fall20_name = "knot_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4" 
if os.path.exists(output_fall20_name):
  os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"knot_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4")


print("================ HIFG RES")

os.chdir("../../../../build")
os.system("./manta ../scenes/knot.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0 "+str(num_frames))
dirct = "output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/"
os.system("./render_statObst.py knot_closeup.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0")
output_fall20_name = "knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_fall20_name):
  os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4")


print("================ Guided")

os.chdir("../../../../build")
os.system("./manta ../scenes/knot.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 4 "+str(num_frames)+" "+str(scale)) # make guided 1/4
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4 "+str(num_frames))
dirct = "output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/"
os.system("./render_statObst.py knot_closeup.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_fall20/knot/knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4")
output_fall20_name= "knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
if os.path.exists(output_fall20_name):
  os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"knot_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4")
