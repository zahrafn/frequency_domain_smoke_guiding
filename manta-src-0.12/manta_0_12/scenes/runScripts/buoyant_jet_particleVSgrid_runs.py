import struct
import sys
import os

res_lowr_x = 64
res_lowr_y = 64 
res_lowr_z = 32

win_sx = 2
win_sy = 2
win_sz = 1

cutoff = 4
num_frames = 250
scale = 3

gridOnly = False
runSims = True
runRenders = True
fastRenders = False

os.chdir("../../build")

if runSims:
  print("================ runs ")
  os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1 0 1") # Low-res with particles
  os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 "+str(cutoff)+" "+str(num_frames)+" "+str(scale)+ " 0 0") # Ideal guiding - grid 00
  os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 "+str(cutoff)+" "+str(num_frames)+" "+str(scale)+ " 1 0") # Gauss - grid 10
  if not gridOnly:
    os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 "+str(cutoff)+" "+str(num_frames)+" "+str(scale)+ " 0 1") # Ideal guiding - particle 01
    os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 "+str(cutoff)+" "+str(num_frames)+" "+str(scale)+ " 1 1") # Gauss - particle 11

if runRenders:

  print("================ rendering ")

  os.chdir("../mitsuba")

  print("================ Ideal guiding - grid 00")
  dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_00/"
  os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+(" 10" if fastRenders else " 1"))

  print("================ Gauss - grid 10") 
  dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_10/"
  os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+(" 10" if fastRenders else " 1"))

  if not gridOnly:
    
    print("================ Ideal guiding - particle 01") 
    dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_01/"
    os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+(" 10" if fastRenders else " 1"))

    print("================ Gauss - particle 11") 
    dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_11/"
    os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+(" 10" if fastRenders else " 1"))

  print("================ make videos")

  os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_00")
  output_spring20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_00.mp4"
  if os.path.exists(output_spring20_name_g):
    os.remove(output_spring20_name_g)
  os.system("ffmpeg -framerate "+("2" if fastRenders else "24")+" -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g)

  os.chdir("../buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_10")
  output_spring20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_10.mp4"
  if os.path.exists(output_spring20_name_g):
    os.remove(output_spring20_name_g)
  os.system("ffmpeg -framerate "+("2" if fastRenders else "24")+" -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g)

  if not gridOnly:
    
    os.chdir("../buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_01")
    output_spring20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_01.mp4"
    if os.path.exists(output_spring20_name_g):
      os.remove(output_spring20_name_g)
    os.system("ffmpeg -framerate "+("2" if fastRenders else "24")+" -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g)

    os.chdir("../buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_11")
    output_spring20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_"+str(cutoff)+"_11.mp4"
    if os.path.exists(output_spring20_name_g):
      os.remove(output_spring20_name_g)
    os.system("ffmpeg -framerate "+("2" if fastRenders else "24")+" -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g)
