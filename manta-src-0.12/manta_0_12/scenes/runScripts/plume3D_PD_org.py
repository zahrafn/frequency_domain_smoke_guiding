import struct
import sys
import os

num_frames = 180
low_res = 32

res_lowr_x = 32
res_lowr_y = 64
res_lowr_z = 32

scale = 3
beta = 4
wScalar = 1


print("================ LOW RES")

os.chdir("../../build")
#os.system("./manta ../scenes/guiding_3d01_low_Z.py "+str(low_res)+" 1 "+str(num_frames))
#os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d_pd/3d01_low "+str(num_frames))
#os.system("./render.py plume3d.xml output_fall19/plume_3d_pd/3d01_low/noise_corrected 0 "+str(num_frames)+" 1")
#
#
#os.chdir("output_fall19/plume_3d_pd/3d01_low/noise_corrected")
#output_fall19_name = "plume_3d_pd_3d01_low.mp4" 
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall19_name)
#
#
#
#
#print("================ HIFG RES Primal_dual Guided")
#os.chdir("../../../../../build")
#os.system("./manta ../scenes/guiding_3d02_high_Z.py "+ str(low_res)+" "+str(scale)+" "+str(wScalar)+" "+str(beta)+" "+str(num_frames))
#os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d_pd/3d02_high "+str(num_frames))
#os.system("./render.py plume3d.xml output_fall19/plume_3d_pd/3d02_high/noise_corrected 0 "+str(num_frames)+" 1")
#
#
#os.chdir("output_fall19/plume_3d_pd/3d02_high/noise_corrected")
#output_fall19_name = "plume_3d_pd_3d02_high.mp4" 
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall19_name)




print("================ LOW RES")


#os.chdir("../../../../../build")
os.system("./manta ../scenes/plume_3d_guiding_for_vsPD.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1")
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d_pd/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d_pd/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d_pd/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected")
output_fall19_name = "plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4")



print("================ HIGH RES")

os.chdir("../../../../../build")
os.system("./manta ../scenes/plume_3d_guiding_for_vsPD.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/noise_corrected")
output_fall19_name = "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_fall19_name):
 os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4")


print("================ Guided 1/6")

os.chdir("../../../../../build")
os.system("./manta ../scenes/plume_3d_guiding_for_vsPD.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 6 "+str(num_frames)+" "+str(scale)) # make guided 1/4
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d_pd/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6/noise_corrected")
output_fall19_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6.mp4")


