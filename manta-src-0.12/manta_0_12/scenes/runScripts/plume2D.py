import struct
import sys
import os


res_lowr_x = 128
res_lowr_y = 128
num_frames = 70
scale = 3
#NOTE: framerate is set to 15 

print("================ LOW RES")


os.chdir("../../build")
#os.system("./manta ../scenes/plume_2d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" 1 0 0 "+str(num_frames)+" 1")
#
#os.chdir("../../../mantaflow_output_fall19/plume2D/plume2d_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"_cutoff0/png")
#output_fall19_name = "plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"_cutoff_0.mp4" 
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 15 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"_cutoff_0.mp4")
#
#
#
#print("================ HIGH RES")
#
#os.chdir("../../../../manta-src-0.12/manta_0_12/build")
#os.system("./manta ../scenes/plume_2d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres
#
#os.chdir("../../../mantaflow_output_fall19/plume2D/plume2d_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff0/png")
#output_fall19_name = "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff_0.mp4"
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 15 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff_0.mp4")
#
#
#print("================ Guided 1/4")
#
#os.chdir("../../../../manta-src-0.12/manta_0_12/build")
os.system("./manta ../scenes/plume_2d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" 0 1 4 "+str(num_frames)+" "+str(scale)) # make guided 1/4

os.chdir("../../../mantaflow_output_fall19/plume2D/plume2d_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff4/png")
output_fall19_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff_4.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 15 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff_4.mp4")

