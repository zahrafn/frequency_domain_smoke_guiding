import struct
import sys
import os

res_lowr_x = 64
res_lowr_y = 64
res_lowr_z = 64
num_frames = 400

print("================ LOW RES")


os.chdir("../../build")
os.system("./manta ../scenes/colliding_plumes_3d.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames))
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
os.system("./render.py colidPlumes.xml output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected")
output_fall19_name = "collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4" 
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4")



print("================ HIFG RES")

os.chdir("../../../../../build")
os.system("./manta ../scenes/colliding_plumes_3d.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 "+str(num_frames)) # make highres
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0 "+str(num_frames))
os.system("./render.py colidPlumes.xml output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0/noise_corrected")
output_fall19_name = "collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0.mp4")


print("================ Guided")

os.chdir("../../../../../build")
os.system("./manta ../scenes/colliding_plumes_3d.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 4 "+str(num_frames)) # make guided 1/4
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4 "+str(num_frames))
os.system("./render.py colidPlumes.xml output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/coliding_plumes/collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4/noise_corrected")
output_fall19_name= "collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4.mp4")


#os.system("ffmpeg -i collidingPlumes_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4 -i collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_4.mp4 -filter_complex hstack collidingPlumes_lowresGuided_+"str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4")
#os.system("ffmpeg -i collidingPlumes_lowresGuided_+"str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4 -i collidingPlumes_"+str(res_lowr_x*2)+"X"+str(res_lowr_y*2)+"X"+str(res_lowr_z*2)+"_cutoff_0.mp4 -filter_complex hstack collidingPlumes_lowresGuidedHighres_collidingPlumes_lowresGuided_+"str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4")
