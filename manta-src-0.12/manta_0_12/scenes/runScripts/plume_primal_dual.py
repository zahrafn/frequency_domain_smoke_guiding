import struct
import sys
import os

num_frames = 180
low_res = 32

res_lowr_x = 32
res_lowr_y = 64
res_lowr_z = 32

win_sx = 2
win_sy = 4
win_sz = 2

scale = 3
beta = 4
wScalar = 1


print("================ LOW RES PD")

os.chdir("../../build")
os.system("./manta ../scenes/guiding_3d01_low_Z.py "+str(low_res)+" 1 "+str(num_frames))


print("================ HIFG RES PD Guided")
os.system("./manta ../scenes/guiding_3d02_high_Z.py "+ str(low_res)+" "+str(scale)+" "+str(wScalar)+" "+str(beta)+" "+str(num_frames))
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/plume_3d_pd/3d02_high "+str(num_frames))
os.system("./render.py plume3d.xml output_spring20/plume/3d02_high/ 0 "+str(num_frames)+" 1")
dirct = "output_spring20/plume/3d02_high/"
os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

os.chdir("output_spring20/plume/3d02_high")
output_spring20_name = "plume_3d02_high.mp4" 
if os.path.exists(output_spring20_name):
  os.remove(output_spring20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name)
