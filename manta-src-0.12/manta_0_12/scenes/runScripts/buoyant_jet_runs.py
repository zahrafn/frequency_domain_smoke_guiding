import struct
import sys
import os

res_lowr_x = 64
res_lowr_y = 64 
res_lowr_z = 32

win_sx = 2
win_sy = 2
win_sz = 1

num_frames = 180
scale = 2
print("================ LOW RES")

os.chdir("../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1 0 1")
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")



os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0_01")
output_spring20_name_l = "buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4" 
if os.path.exists(output_spring20_name_l):
  os.remove(output_spring20_name_l)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_l)


print("================ HIFG RES")

os.chdir("../../../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 "+str(num_frames)+" "+str(scale)+" 0 1") # make highres
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")



os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0_01")
output_spring20_name_h = "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_spring20_name_h):
  os.remove(output_spring20_name_h)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_h)



print("================ Guided 4")

os.chdir("../../../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 4 "+str(num_frames)+" "+str(scale)+" 0 1") # make guided 1/4
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")



os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4_01")
output_spring20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
if os.path.exists(output_spring20_name_g):
  os.remove(output_spring20_name_g)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g)
'''


print("================ Guided 2")
os.chdir("../../../../build")
##os.chdir("../../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 2 "+str(num_frames)+" "+str(scale)+" 0 1") # make guided 1/2
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2_01")
output_spring20_name_g2= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2.mp4"
if os.path.exists(output_spring20_name_g2):
  os.remove(output_spring20_name_g2)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g2)

print("================ Guided 6")

os.chdir("../../../../build")
#os.chdir("../../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 6 "+str(num_frames)+" "+str(scale)+" 0 1") # make guided 1/6
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6_01")
output_spring20_name_g6= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_6.mp4"
if os.path.exists(output_spring20_name_g6):
  os.remove(output_spring20_name_g6)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g6)


print("================ Guided 8")

os.chdir("../../../../build")
#os.chdir("../../../build")
os.system("./manta ../scenes/buoyant_jet.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 8 "+str(num_frames)+" "+str(scale)+" 0 1") # make guided 1/8
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8 "+str(num_frames))
dirct = "output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8_01/"
os.system("./render_param.py buoyant_jet.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


os.chdir("output_spring20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8_01")
output_spring20_name_g8= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8.mp4"
if os.path.exists(output_spring20_name_g8):
  os.remove(output_spring20_name_g8)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_spring20_name_g8)


'''
