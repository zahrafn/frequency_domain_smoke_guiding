import struct
import sys
import os

res_lowr_x = 64
res_lowr_y = 64 
res_lowr_z = 32

win_sx = 2
win_sy = 2
win_sz = 1

num_frames = 180
scale = 3



print("================ runs ")
os.chdir("../../build")
# Ideal guiding - particle 0 1
#os.system("./manta ../scenes/buoyant_jet_lowhighdifftest.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1") 
#os.system("./manta ../scenes/buoyant_jet_lowhighdifftest.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 0 "+str(num_frames)+" "+str(scale)) 
#os.system("./manta ../scenes/buoyant_jet_lowhighdifftest.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 4 "+str(num_frames)+" "+str(scale))



print("================ rendering ")


os.chdir("../mitsuba")

#dirct = "output_fall20/buoyant_jet/buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/"
#os.system("./render_param_wtracer.py buoyant_jet_wtracer.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

'''
dirct ="output_fall20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/"
os.system("./render_param_wtracer.py buoyant_jet_wtracer.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

dirct = "output_fall20/buoyant_jet/buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/"
os.system("./render_param_wtracer.py buoyant_jet_wtracer.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")


'''
print("================ make videos")

print("================ low(guid vel) source on left")
os.chdir("output_fall20/buoyant_jet/buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0")
#output_fall20_name_g= "buoyant_jet_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4"
#if os.path.exists(output_fall20_name_g):
#  os.remove(output_fall20_name_g)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall20_name_g)

print("================ high source on right") 
os.chdir("../buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0")
output_fall20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_fall20_name_g):
  os.remove(output_fall20_name_g)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall20_name_g)

print("================ guided source on right") 
os.chdir("../buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4")
output_fall20_name_g= "buoyant_jet_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
if os.path.exists(output_fall20_name_g):
  os.remove(output_fall20_name_g)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall20_name_g)

