import struct
import sys
import os

num_frames = 180

res_lowr_x = 32
res_lowr_y = 64
res_lowr_z = 32

win_sx = 2
win_sy = 4
win_sz = 2

scale = 4




print("================ LOW RES")
os.chdir("../../build")
os.system("./manta ../scenes/plum_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 0 0 "+str(num_frames)+" 1")

print("================ HIGH RES")

os.system("./manta ../scenes/plum_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres




