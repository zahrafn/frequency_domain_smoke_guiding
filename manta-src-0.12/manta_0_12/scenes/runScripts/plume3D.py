import struct
import sys
import os

#res_lowr_x = 70
#res_lowr_y = 140
#res_lowr_z = 70
#num_frames = 400


res_lowr_x = 32
res_lowr_y = 64
res_lowr_z = 32
num_frames = 120

scale = 3

print("================ LOW RES")


os.chdir("../../build")
os.system("./manta ../scenes/plume_3d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 "+str(num_frames)+" 1")
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/noise_corrected")
output_fall19_name = "plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4" 
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4")



print("================ HIGH RES")

os.chdir("../../../../../build")
os.system("./manta ../scenes/plume_3d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/noise_corrected")
output_fall19_name = "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4")


print("================ Guided 1/4")

os.chdir("../../../../../build")
os.system("./manta ../scenes/plume_3d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 4 "+str(num_frames)+" "+str(scale)) # make guided 1/4
os.chdir("../mitsuba")
os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4 "+str(num_frames))
os.system("./render.py plume3d.xml output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/noise_corrected 0 "+str(num_frames)+" 1")


os.chdir("output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/noise_corrected")
output_fall19_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
if os.path.exists(output_fall19_name):
  os.remove(output_fall19_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4")

#os.system("ffmpeg -i plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4 -i plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4 -filter_complex hstack plume_lowresGuided_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4")
#os.system("ffmpeg -i plume_lowresGuided_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4 -i plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4 -filter_complex hstack plume_lowresGuidedHighres_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+".mp4")
#print("================ Guided 1/2")
#
#os.chdir("../../../../../build")
#os.system("./manta ../scenes/plume_3d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 2 "+str(num_frames)+" "+str(scale)) # make guided 1/4
#os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2 "+str(num_frames))
#os.system("./render.py plume3d.xml output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2/noise_corrected 0 "+str(num_frames)+" 1")
#
#
#os.chdir("output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2/noise_corrected")
#output_fall19_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2.mp4"
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_2.mp4")
#
#
#
#print("================ Guided 1/8")
#
#os.chdir("../../../../../build")
#os.system("./manta ../scenes/plume_3d_guiding.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 8 "+str(num_frames)+" "+str(scale)) # make guided 1/4
#os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8 "+str(num_frames))
#os.system("./render.py plume3d.xml output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8/noise_corrected 0 "+str(num_frames)+" 1")
#
#
#os.chdir("output_fall19/plume_3d/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8/noise_corrected")
#output_fall19_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8.mp4"
#if os.path.exists(output_fall19_name):
#  os.remove(output_fall19_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_8.mp4")

