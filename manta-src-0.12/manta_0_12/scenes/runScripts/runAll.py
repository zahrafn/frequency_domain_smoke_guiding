import struct
import sys
import os


print(":::::::::::::::: Plume ::::::::::::::::")
os.system("python plume_pd_upsampl_gaussflt_runs.py")

print(":::::::::::::::: buoyant Jet ::::::::::::::::")
os.system("python buoyant_jet_runs.py")

print(":::::::::::::::: Sphere ::::::::::::::::")
os.system("python sphere_runs.py")

print(":::::::::::::::: knot ::::::::::::::::")
os.system("python knot_runs.py")

print(":::::::::::::::: six_cylinder ::::::::::::::::")
os.system("python six_cylinder_runs.py")

print(":::::::::::::::: Moving Cylinder ::::::::::::::::")
os.system("python moving_cylinder_runs.py")


