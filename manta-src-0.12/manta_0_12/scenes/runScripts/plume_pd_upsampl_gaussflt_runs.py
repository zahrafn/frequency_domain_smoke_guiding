import struct
import sys
import os

num_frames = 180
low_res = 32

res_lowr_x = 32
res_lowr_y = 64
res_lowr_z = 32

win_sx = 2
win_sy = 4
win_sz = 2

scale = 3
beta = 4
wScalar = 1




print("================ LOW RES")
os.chdir("../../build")
os.system("./manta ../scenes/plume_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 1 0 0 0 0 "+str(num_frames)+" 1")
os.chdir("../mitsuba")
##os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0 "+str(num_frames))
dirct = "output_fall20/plume/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0/"
os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

os.chdir("output_fall20/plume/plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0")
output_fall20_name = "plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4"
if os.path.exists(output_fall20_name):
  os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x)+"X"+str(res_lowr_y)+"X"+str(res_lowr_z)+"_cutoff_0.mp4")



print("================ HIGH RES")

os.chdir("../../../../build")
os.system("./manta ../scenes/plume_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 0 0 "+str(num_frames)+" "+str(scale)) # make highres
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0 "+str(num_frames))
dirct = "output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0/"
os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

os.chdir("output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0")
output_fall20_name = "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4"
if os.path.exists(output_fall20_name):
 os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_0.mp4")


print("================ Guided 1/4")

os.chdir("../../../../build")
os.system("./manta ../scenes/plume_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 0 1 4 "+str(num_frames)+" "+str(scale)) # make guided 1/4
os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4 "+str(num_frames))
dirct = "output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/"
os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

os.chdir("output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4")
output_fall20_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
if os.path.exists(output_fall20_name):
  os.remove(output_fall20_name)
os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4")


#print("================ upsamp")
#
#os.chdir("../../../../build")
#os.system("./manta ../scenes/plume_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 1 0 0 1 "+str(num_frames)+" "+str(scale)) # make guided 1/4
#os.chdir("../mitsuba")
##os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1 "+str(num_frames))
#dirct = "output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1/"
#os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")
#
#os.chdir("output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1")
#output_fall20_name= "plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1.mp4"
#if os.path.exists(output_fall20_name):
#  os.remove(output_fall20_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1.mp4")


#print("================ gaussian filter")
#
#os.chdir("../../../../build")
#os.system("./manta ../scenes/plume_pd_upsampl_gaussflt.py "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" 0 0 1 0 4 "+str(num_frames)+" "+str(scale)) # make guided 1/4
#os.chdir("../mitsuba")
#os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/plume_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_1 "+str(num_frames))
#dirct = "output_fall20/plume/plume_gauss_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4/"
#os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")

#os.chdir("output_fall20/plume/plume_gauss_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4")
#output_fall20_name= "plume_gauss_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4"
#if os.path.exists(output_fall20_name):
#  os.remove(output_fall20_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+"plume_gauss_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"X"+str(res_lowr_z*scale)+"_cutoff_4.mp4")
##
#print("================ LOW RES PD")
#
#os.chdir("../../../../build")
#os.system("./manta ../scenes/guiding_3d01_low_Z.py "+str(low_res)+" 1 "+str(num_frames))
#os.chdir("../mitsuba")
##os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/3d01_low "+str(num_frames))
#dirct = "output_fall20/plume/3d01_low/"
#os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x)+" "+str(res_lowr_y)+" "+str(res_lowr_z)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")
#
#os.chdir("output_fall20/plume/3d01_low")
#output_fall20_name = "plume_3d01_low.mp4" 
#if os.path.exists(output_fall20_name):
#  os.remove(output_fall20_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall20_name)


#print("================ HIFG RES PD Guided")
#os.chdir("../../../../build")
#os.system("./manta ../scenes/guiding_3d02_high_Z.py "+ str(low_res)+" "+str(scale)+" "+str(wScalar)+" "+str(beta)+" "+str(num_frames))
#os.chdir("../mitsuba")
##os.system("python noiseCorrectionAllFrames.py noiseCorrection.py output_fall20/plume/3d02_high "+str(num_frames))
#dirct = "output_fall20/plume/3d02_high/"
#os.system("./render_param.py plume3d_param.xml "+dirct+ " "+str(res_lowr_x*scale)+" "+str(res_lowr_y*scale)+" "+str(res_lowr_z*scale)+" "+str(win_sx)+" "+str(win_sy)+ " "+str(win_sz)+" 0 "+str(num_frames)+" 1")
#
#os.chdir("output_fall20/plume/3d02_high")
#output_fall20_name = "plume_3d02_high.mp4" 
#if os.path.exists(output_fall20_name):
#  os.remove(output_fall20_name)
#os.system("ffmpeg -framerate 24 -i %*.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "+output_fall20_name)



