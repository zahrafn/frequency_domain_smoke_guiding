#
# Simple example scene for a 2D circularly guided simulation
# Main params:
# 	W = guiding weight, different strengths for top / bottom halves
# 	beta = blur radius
#	scale = up-res factor
#
from manta import *
import math
import os
import time
import sys

args = len(sys.argv)
if args < 4:
    print("incorrect number of arguments")
    sys.exit()

res0= int(sys.argv[1])
scale = int(sys.argv[2])
beta= float(sys.argv[3])
num_frames = int(sys.argv[4])



# solver params
# num_frames = 200
# res0 = 128
# scale = 1
res = res0*scale
gs = vec3(res,res,1)
s = Solver(name='main', gridSize = gs, dim=2)
s.timestep = 2.0/scale
timings = Timings()
sim_time = [None]*num_frames
guide_time = [None]*num_frames
rest_time = [None]*num_frames

# IOP (=1), ADMM (=2) or PD (=3)

# params
valAtMin = 1
valAtMax = 1
# beta = 2
output_ppm = '../../../mantaflow_output/circularVelocity/guided_'+str(res)+'_w1beta'+str(beta)+'/ppm/cv_%04d.ppm'
output_png = '../../../mantaflow_output/circularVelocity/guided_'+str(res)+'_w1beta'+str(beta)+'/png/cv_%04d.png'
tau = 1.0
sigma = 0.99/tau
theta = 1.0

imageDir1 = os.path.dirname(output_ppm)
if not os.path.exists(imageDir1):
	os.makedirs(imageDir1)

imageDir2 = os.path.dirname(output_png)
if not os.path.exists(imageDir2):
	os.makedirs(imageDir2)

# prepare grids
flags = s.create(FlagGrid)
vel = s.create(MACGrid)
velT = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
W = s.create(RealGrid)

bWidth=1
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

if (GUI):
	gui = Gui()
	gui.show()
	gui.nextVec3Display()
	gui.nextVec3Display()
	gui.nextVec3Display()
#gui.pause()

source = s.create(Cylinder, center=gs*vec3(0.5,0.2,0.5), radius=gs.y*0.14, z=gs*vec3(0, 0.02*1.5, 0))
getSpiralVelocity2D(flags=flags, vel=velT, strength=0.5)

setGradientYWeight(W=W, minY=0,     maxY=res/2, valAtMin=valAtMin, valAtMax=valAtMin)
setGradientYWeight(W=W, minY=res/2, maxY=res,   valAtMin=valAtMax, valAtMax=valAtMax)

#main loop
for t in range(200):

	start = time.clock()
	resetOutflow(flags=flags,real=density)

	if t<120:
		source.applyToGrid(grid=density, value=1.5)

	advectSemiLagrange(flags=flags, vel=vel, grid=density, order=2)
	advectSemiLagrange(flags=flags, vel=vel, grid=vel,     order=2)

	setWallBcs(flags=flags, vel=vel)
	addBuoyancy(density=density, vel=vel, gravity=vec3(0,-0.5e-3,0), flags=flags)

	start_g = time.clock()

	PD_fluid_guiding(vel=vel, velT=velT, flags=flags, weight=W, blurRadius=beta, pressure=pressure, \
					 tau = tau, sigma = sigma, theta = theta, preconditioner = PcMGStatic, zeroPressureFixing=True )

	end_g = time.clock()
	guide_time[t] = (end_g-start_g)*1000.
	setWallBcs(flags=flags, vel=vel)

	#timings.display()
	s.step()

	end = time.clock()
	sim_time[t] = (end-start)*1000.
	rest_time[t] = ((end - start) - (end_g - start_g))*1000.

	if 1:
		projectPpmFull( density, output_ppm % t , 0, 1.0 );
		# gui.screenshot(output_png % t)

ts = float("%.4f"% (sum(sim_time) / float(len(sim_time))))
tr = float("%.4f"% (sum(rest_time) / float(len(rest_time))))
tg = float("%.4f"% (sum(guide_time) / float(len(guide_time))))


#print("ts, tg, tr ",ts, tg, tr);
#file = open("runTime/ueanTimes_circularVel_PDGuiding_"+str(beta)+".txt","w")
#file.write("res, beta: tg & tgp & ts "+str(res)+", "+ str(beta)+": "+ str(tg)+ " & "+ str(tpg)+" & "+str(ts))
#file.close()

