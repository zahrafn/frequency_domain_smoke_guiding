from manta import *
import os
from timeit import default_timer as timer
import sys


args = len(sys.argv)
if args < 7:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])

cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])

if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0

if unguide_lowres:
    scale =1
else:
    scale = 2
gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)
res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale
gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)

output_fall19Dir = "../../../mantaflow_output_fall19/collidingPlumes3D/"
guide_vel_dir = output_fall19Dir + "lowResVelocity3D/";

mitsubaoutdir ="../mitsuba/output_fall19/coliding_plumes/collidingPlumes_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))
# mitsubaoutdir = "../mitsuba/output_fall19/plume/highres"
# mitsubaoutdir = "../mitsuba/output_fall19/plume/guided128_cutoff2"
# mitsubaoutdir = "../mitsuba/output_fall19/plume/guided128_cutoff4"
# mitsubaoutdir = "../mitsuba/output_fall19/plume/guided128_cutoff8"

# solver params


s = Solver(name='highRes', gridSize = gs_highr, dim=3)

if not os.path.exists(guide_vel_dir):
    os.makedirs(guide_vel_dir)
    os.makedirs(guide_vel_dir+'vx')
    os.makedirs(guide_vel_dir+'vy')
    os.makedirs(guide_vel_dir+'vz')

s.timestep = 1.0
timings = Timings()

# prepare grids
flags = s.create(FlagGrid)
vel = s.create(MACGrid)
vel_guide = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)



# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(45)
noise.clamp = True
noise.clampNeg = 1
noise.clampPos = 1
noise.valOffset = 0.5
noise.timeAnim = 0.2

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth, 'xXyYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show( True )
    # gui.nextVec3Display()

# gui.pause()
if unguide_lowres:
    if not os.path.exists(guide_vel_dir):
        os.makedirs(guide_vel_dir)
        os.makedirs(guide_vel_dir+'vx')
        os.makedirs(guide_vel_dir+'vy')
        os.makedirs(guide_vel_dir+'vz')

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)

def pulse(t, start, end):
    if t < start or t > end:
        return 0.
    return (4.*(t - start)*(end - t)/(end - start)**2)**3

source1 = s.create(Cylinder, center= gs_highr * vec3(0.15, 0.15, 0.5), radius=res_highr_x * 0.05, z=gs_highr * vec3(0.02, 0.02, 0))
source2 = s.create(Cylinder, center=gs_highr * vec3(0.85, 0.15, 0.5), radius=res_highr_x * 0.05, z=gs_highr * vec3(-0.02, 0.02, 0))

#main loop
for t in range(num_frames):
    mantaMsg('\nFrame %i' % (s.frame))

    # start = timer()

    advectSemiLagrange(flags=flags, vel=vel, grid=density, order=2, orderSpace=2, orderTrace=2)
    advectSemiLagrange(flags=flags, vel=vel, grid=vel, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
    resetOutflow(flags=flags, real=density)

    vel1 = pulse(t, 0, 50)*vec3(0.04,0.04,0)*gs_highr
    vel2 = pulse(t, 10, 70)*vec3(-0.04,0.04,0)*gs_highr
    # vel1 = pulse(t, 0, 50)*vec3(0.8,0.8,0)*scale


    densityInflow(flags=flags, density=density, noise=noise, shape=source1, scale=1, sigma=0.5)
    velocityInflow(flags=flags, vel=vel, velToSet=vel1, noise=noise, shape=source1, sigma=0.5)
    densityInflow(flags=flags, density=density, noise=noise, shape=source2, scale=1, sigma=0.5)
    velocityInflow(flags=flags, vel=vel, velToSet=vel2, noise=noise, shape=source2, sigma=0.5)


    #   for Mitsuba rendering
    density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
    # vel.save('%s/velocity_%04d.uni' % (outdir, t))

    setWallBcs(flags=flags, vel=vel)
    addBuoyancy(density=density, vel=vel, gravity=vec3(0,-1e-4,0), flags=flags)

#    solvePressure(flags=flags, vel=vel, pressure=pressure)

    if guide==0:
        solvePressure(flags=flags, vel=vel, pressure=pressure)

    #timings.display()
    s.step()

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel_dir, vel=vel,res = gs_highr,frameNum=t,binary=False)

    elif guide:
	#readInMACGrid(folderName=guide_vel_dir, vel=vel, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=True)
	#ideal_filtering_smoke_guiding(veles=vel,vel_guidees=vel_guide, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)
        readInMACGrid(folderName=guide_vel_dir, vel=vel_guide, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        ideal_filtering_smoke_guiding(vel_lowres=vel_guide,vel_highres=vel, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)
        setWallBcs(flags=flags, vel=vel)
        solvePressure(flags=flags, vel=vel, pressure=pressure)

