from manta import *
import os
from timeit import default_timer as timer

# to generate the low-res guide velocities      scale = 1 guideGenerator = True
# to generate the high-res unguided             scale = 2 guideGenerator = True
# to generate the high-res guided               scale = 2 guideGenerator = False ->set the cutoff

unguide_lowres= True # make sure to be False for guiding
guide =  False

num_frames = 200
res_lowr_x = 100
res_lowr_y = 120
scale =1;
cutoff = 1/2.

if unguide_lowres:
    scale =1
else:
    scale = 2

outputDir = "../../../mantaflow_output/colidingPlumes2D/"
guide_vel = outputDir + "lowResVelocity2D/";
output_ppm = outputDir +'guided200X240_cutoff2/ppm/pl_%04d.ppm'
output_png = outputDir +'guided200X240_cutoff2/png/pl_%04d.png'



##################### low-res plume

gs_lowr = vec3(res_lowr_x,res_lowr_y,1)

res_highr_x = res_lowr_x * scale
res_highr_y = res_lowr_y * scale

gs_highr = vec3(res_highr_x,res_highr_y,1)
s = Solver(name='highRes', gridSize = gs_highr, dim=2)
s.timestep =1.0
timings = Timings()

if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')


imageDir1 = os.path.dirname(output_ppm)
imageDir2 = os.path.dirname(output_png)
if not os.path.exists(imageDir1):
    os.makedirs(imageDir1)
if not os.path.exists(imageDir2):
    os.makedirs(imageDir2)

s.timestep = 1.0
timings = Timings()

# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure= s.create(RealGrid)



# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(45)
noise.clamp = True
noise.clampNeg = 1
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth,'xXyYzZ',FlagOutflow|FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show( True )
    # gui.nextVec3Display()

# gui.pause()

outdir = "../mitsuba/output/plume_lowres64"
if not os.path.exists(outdir):
    os.makedirs(outdir)

source1 = s.create(Cylinder, center=float(res_lowr_x*scale)*vec3(0.2,0.15,0), radius=res_lowr_x*scale*0.05, z=float(res_lowr_x*scale)*vec3(0.01, 0.02, 0))
source2 = s.create(Cylinder, center=float(res_lowr_x*scale)*vec3(0.8,0.15,0), radius=res_lowr_x*scale*0.05, z=float(res_lowr_x*scale)*vec3(-0.01, 0.02, 0))

def pulse(t, start, end):
    if t < start or t > end:
        return 0.
    return (4.*(t - start)*(end - t)/(end - start)**2)**3


#main loop
for t in range(num_frames):
    mantaMsg('\nFrame %i' % (s.frame))

    start = timer()

    advectSemiLagrange(flags=flags, vel=vel_highr, grid=density, order=2, orderSpace=2, orderTrace=2)
    advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
    resetOutflow(flags=flags,real=density)

    vel1 = pulse(t, 0, 50)*vec3(0.02,0.04,0)*float(res_lowr_x)*scale
    vel2 = pulse(t, 25, 75)*vec3(-0.02,0.04,0)*float(res_lowr_x)*scale

    densityInflow(flags=flags, density=density, noise=noise, shape=source1, scale=1, sigma=0.5)
    velocityInflow(flags=flags, vel=vel_highr, velToSet=vel1, noise=noise, shape=source1, sigma=0.5)
    densityInflow(flags=flags, density=density, noise=noise, shape=source2, scale=1, sigma=0.5)
    velocityInflow(flags=flags, vel=vel_highr, velToSet=vel2, noise=noise, shape=source2, sigma=0.5)

    setWallBcs(flags=flags, vel=vel_highr)
    # addBuoyancy(density=density_lowr, vel=vel_lowr, gravity=vec3(0,-4e-3,0), flags=flags_lowr)

    solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

    #timings.display()
    s.step()

    # writeOutMACGrid(folderName=guide_vel, vel=vel,res = gs_lowr,frameNum=t,binary=True)

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t,binary=True)

    elif guide:
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=True)
        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)



    if 1:# and t%scale==0:
        projectPpmFull( density, output_ppm % t , 0, 1.0 );
        gui.screenshot(output_png % t);