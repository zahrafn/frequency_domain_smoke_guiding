#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args < 8:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])


cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])
scale = int(sys.argv[8])

num_pressGuidIter = int(sys.argv[9])

print(res_lowr_x , res_lowr_y, res_lowr_z, unguide_lowres, guide, cutoffD, num_frames)


if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0


useReflectionMethod = False
# to generate the low-res guide velocities      unguide_lowres = True    guide = False
# to generate the high-res unguided             unguide_lowres = False   guide = False
# to generate the high-res guided               unguide_lowres = False   guide = True

timings = Timings()
sim_time = [None]*num_frames
guide_time = [None]*num_frames
guidePress_time = [None]*num_frames


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)


outputDir = "../../../mantaflow_output_fall19/plume3D_pgitr/"
guide_vel = outputDir + "lowResVelocity3D/";

mitsubaoutdir = "../mitsuba/output_fall19/plume_pgiter/plume_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))
diff_vel_output = outputDir +"diffVel/"
folderName_div = outputDir + "div/"+str(num_pressGuidIter)+"itr/";

s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()

# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
vel_highr_pre = s.create(MACGrid)
diff_vel= s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

div_grid = s.create(RealGrid)
#div_after_guiding = s.create(RealGrid)
#div_before_press = s.create(RealGrid)
#div_after_guiding_press=s.create(RealGrid)


# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
#noise.posScale = vec3(45)
noise.posScale = vec3(50)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.8
noise.timeAnim = 0.2

bWidth= 0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

#setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()

if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')
        os.makedirs(guide_vel+'vz')

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)
    os.makedirs(diff_vel_output)
    os.makedirs(folderName_div)

source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.11, z=gs_highr * vec3(0, 0.02, 0))
#buoyancy = vec3(0, -8e-3, 0)

buoyancy = vec3(0, 1e-3, 0)

#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)

    #setWallBcs(flags=flags, vel=vel_highr)
    #addBuoyancy(density=density, vel=vel_highr, gravity=vec3(0.0,-1e-4,0.0), flags=flags)

        # divRealGrid(div=div_before_press ,grid=vel_highr)
        # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)
    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

    # divRealGrid(div=div_before_press ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)

    if guide==0:
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t


#main loop
for t in range(num_frames):
    mantaMsg('\n################## Frame %i' % (s.frame))
    t = step(t)
    
    if guide==0:
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)


    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t,binary=False)

    elif guide:
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
             
        for itr in range(num_pressGuidIter):
            #print("Frame: ", t, " itr:", itr)
            #vel_highr.printGrid()
            
            #ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
            if t == 32 :
                velDiff(vel_highr,vel_highr_pre,diff_vel)
                writeOutMACGrid(folderName=diff_vel_output, vel=diff_vel ,res = gs_highr,frameNum=itr,binary=False)
                divRealGrid(div=div_grid ,grid=vel_highr)
                writeRealGrid(folderName_div, div_grid, gs_highr, itr)
            
            setWallBcs(flags=flags, vel=vel_highr)
            solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
            ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        

            vel_highr_pre.copyFrom(vel_highr)
                       
           # print("End itr")
           # vel_highr.printGrid()
        
          
        #print("------>end")
        #vel_highr.printGrid()

       # write out to file
       # if t == 30:
    #divRealGrid(div=div_grid ,grid=vel_highr)
    #writeRealGrid(folderName_div, div_grid, gs_highr,t)



