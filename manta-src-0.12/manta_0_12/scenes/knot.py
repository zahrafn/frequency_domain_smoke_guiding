#
#
from manta import *
import os
import sys
import time

args = len(sys.argv)
if args < 8:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])

cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])

scale = int(sys.argv[8])
num_pressGuidIter = 1 #int(sys.argv[9])


if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0

gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)

output_fall20Dir = "../../../mantaflow_output_fall20/plume3D_knot/"
guide_vel = output_fall20Dir + "lowResVelocity3D/";
cgiter = output_fall20Dir + "cgiternum/";

mitsubaoutdir = "../mitsuba/output_fall20/knot/knot_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))

output_runtime = "../mitsuba/output_fall20/runtime_data"

# mesh to load
#meshfile = '../resources/simpletorus.obj'
#meshfile = '../resources/knot.obj'  
meshfile = '../resources/saddletref.obj'
mantaMsg("Loading %s. Note: relative path by default, assumes this scene is called from the 'scenes' directory.")

s= Solver(name='lowRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()

if not os.path.exists(guide_vel):
    os.makedirs(guide_vel)
    os.makedirs(guide_vel+'vx')
    os.makedirs(guide_vel+'vy')
    os.makedirs(guide_vel+'vz')


# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
vel_highr_lowPassed = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
phiObs   = s.create(LevelsetGrid)
mesh     = s.create(Mesh)



#### Density tracking with particles ####

# Particle sampling settings
useParticles = True
discretization = 3
randomness = 0.5
# Data structures
density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False, stopInObstacle=False)
        pushOutofObs(parts=pp, flags=flags, phiObs=phiObs)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)



# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(45)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2


# noise field, tweak a bit for smoke source
noise2 = s.create(NoiseField, loadFromFile=True)
noise2.posScale = vec3(15)
noise2.clamp = True
noise2.clampNeg = 0.5
noise2.clampPos = 1
noise2.valOffset = 0.75
noise2.timeAnim = 1.0


bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()


if (GUI):
    gui = Gui()
    gui.show( True )
#gui.pause()

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)

source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.05, 0.5), radius=res_highr_x * 0.1, z=gs_highr * vec3(0, 0.02, 0))


# === load mesh, and turn into SDF ===
mesh.load( meshfile )
mscale = 0.4
meshScale = mscale * res_highr_x
mesh.scale( vec3(meshScale) )
ofs = vec3(0.0,0.0,0.0)
offSet = gs_highr*  (vec3(0.5)+ ofs)
mesh.offset( offSet ) # center + slight offset
mesh.computeLevelset(phiObs, 2.)

flags.initDomain()
setObstacleFlags(flags=flags, phiObs=phiObs) #, fractions=fractions)
flags.fillGrid()

setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)


fs = open("%s/obstSize.txt" % (mitsubaoutdir),'w')
fs.write("%5.4f" % (mscale))

fp = open("%s/obstPos.txt" % (mitsubaoutdir),'w')
fp.write("%5.4f %5.4f %5.4f" % (ofs.x,ofs.y,ofs.z))

print('mscale: ', mscale, ' pos: ', ofs.x, ofs.y, ofs.z)

#flags.fillGrid()

buoyancy = vec3(0, 8e-4, 0)
useReflectionMethod = True

save_time = True
step_time =[]
guide_time =[]
lastpress_time =[]
sim_time = []
all_pressIter = []
stop1s = stop1e = stop2s = stop2e = stop3s = stop3e = stop4s = stop4e = endguide = startguide = startlastpress = endlastpress =0.0

pressIter_tot =0
endPress = True
#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        #velocityInflow(flags=flags, vel=vel_highr, velToSet=vec3(-3e-3, 0, 0) * float(res_highr_x), noise=noise2, shape=source, sigma=0.5)
        advectSemiLagrange_p(flags=flags, vel=vel_highr, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)

    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        if save_time:
            stop3s = time.clock()
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        if save_time:
            stop3e = time.clock()
        #velocityInflow(flags=flags, vel=vel_highr, velToSet=vec3(-3e-3, 0, 0) * float(res_highr_x), noise=noise2, shape=source, sigma=0.5)

      # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)

        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)#, pressIter = pressIter)
        
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        if save_time:
            stop4s = time.clock()
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        if save_time:
            stop4e = time.clock()
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

    if not endPress:
        if guide==0:
            setWallBcs(flags=flags, vel=vel_highr)
            solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t




#main loop
t = 0
while t < num_frames:
    cgiter_filename = cgiter+'frame_%d.txt' % ( t)
    mantaMsg('\n################## Frame %i' % (s.frame))
    start = time.clock()
    if save_time:
        startstep = time.clock()
    t = step(t)
    if save_time:
        endstep = time.clock()
    

    #s.step()

    if unguide_lowres:
        if save_time:
            stop1s = time.clock()
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t, binary=False)
        if save_time:
            stop1e = time.clock()
    elif guide:

        if save_time:
            stop2s = time.clock()
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        if save_time:
            stop2e = time.clock()
        
        if save_time:
            startguide = time.clock()
        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        if save_time:
            endguide = time.clock()

        if not endPress:
            if save_time:
                startlastpress = time.clock()
            setWallBcs(flags=flags, vel=vel_highr)
            solvePressure(flags=flags, vel=vel_highr, pressure=pressure)#,  pressIter = pressIter)
            if save_time:
                endlastpress = time.clock()



    if endPress:
        if save_time:
            startlastpress = time.clock()
        setWallBcs(flags=flags, vel=vel_highr)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)#,  pressIter = pressIter)
        if save_time:
            endlastpress = time.clock()


    end = time.clock()
    steptime = (endstep- startstep) + (endlastpress - startlastpress)
    guidetime = endguide- startguide
    #lastPresstime = endlastpress - startlastpress
    
    stime = end-start - (stop1e - stop1s) - (stop2e - stop2s) - (stop3e - stop3s) - (stop4e - stop4s)
    sim_time.append((stime))
    step_time.append((steptime))
    guide_time.append((guidetime))
    #all_pressIter.append(pressIter_tot)
    #lastpress_time.append((lastPresstime))

    print(stime, steptime, guidetime)

ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))
tst= float("%.2f"% (sum(step_time) / float(len(step_time))))
tg = float("%.2f"% (sum(guide_time) / float(len(guide_time))))
print('[resx resy resz  cutoff tg ts tst tg ]',res_highr_x,res_highr_y,res_highr_z, cutoff, ts, tst, tg)
#file = open(output_runtime+"/meanTimes_knot_"+str(res_highr_x)+"_"+str(cutoff)+".txt","w")
#file.write("resx, resy, resz, cutoff: ts tstep tguide "+str(res_highr_x)+", "+str(res_highr_y)+", "+str(res_highr_z)+", "+ str(cutoff)+"- "+": "+str(ts)+ " "+str(tst)+" "+str(tg))
#file.close()

