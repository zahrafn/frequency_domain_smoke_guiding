#
# Simulation of a 3D buoyant smoke density plume 
# guided by a low res sim 
#
# parameterss:
#   W = guiding weight (constant value of wScalar)
#   beta = blur radius
#
from manta import *
import sys
import time

# params from original simulation
# res1 = before interp, res2 = after interp

args = len(sys.argv)
if args < 5:
    print("incorrect number of arguments")
    sys.exit()

res1= int(sys.argv[1])
factor = int(sys.argv[2])
# PD params
wScalar = int(sys.argv[3])
beta = int(sys.argv[4])
numFrames = int(sys.argv[5])


#timestep = 0.65
timestep =1.0
#res1 = 70
#numFrames = 400
#factor = 2
res2 = int(res1*factor)

# solver params
gs2 = vec3(res2,int(2.0*res2),res2)
#gs2 = vec3(res2,res2,res2)
s2 = Solver(name='main', gridSize = gs2, dim=3)
s2.timestep = timestep
timings = Timings()


mitsubaoutdir = '../mitsuba/output_fall20/plume/3d02_high/'

#input_uni  = '../mitsuba/output_fall20/plume/3d01_low/plume3DLowRes_%04d.uni'
# output_fall20_ppm = mitsubaoutdir+'/plume3DHighRes_%04d.ppm'
input_uni  = '../mitsuba/output_fall20/plume/plume_32X64X32_cutoff_0/velocity_low_%04d.uni'
output_fall20_uni = '../mitsuba/output_fall20/plume/3d02_high/plume3DHighRes_%04d.uni'

output_runtime = "../mitsuba/output_fall20/runtime_data"

# PD params
#beta = 5
#wScalar = 2

tau = 0.58/wScalar
sigma = 2.44/tau
theta = 0.3



# prepare grids
flags = s2.create(FlagGrid)
vel = s2.create(MACGrid)
velT = s2.create(MACGrid)
density = s2.create(RealGrid)
pressure = s2.create(RealGrid)
W = s2.create(RealGrid)

gsLoad = vec3(res1,int(2*res1),res1)
sLoader = Solver(name='main', gridSize = gsLoad, dim=3) 
velIn = sLoader.create(MACGrid)


#### Density tracking with particles ####

# Particle sampling settings
useParticles = False
discretization = 3
randomness = 0.5
# Data structures
density_change = s2.create(RealGrid)
pp = s2.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)


# noise field
noise = s2.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
#noise.valScale = 1
noise.valOffset = 0.75
noise.timeAnim = 0.5

bWidth=0
flags.initDomain(boundaryWidth=bWidth) 
flags.fillGrid()
setOpenBound(flags,bWidth,'xXYzZ',FlagOutflow|FlagEmpty) 

if (GUI):
        gui = Gui()
        gui.show()
        gui.nextVec3Display() # hide velocity display
        gui.nextVec3Display()
        gui.nextVec3Display()
        #gui.pause()


source = s2.create(Cylinder, center=gs2*vec3(0.5,0.04,0.5), radius=res2*0.17, z=gs2*vec3(0, 0.02, 0))
W.multConst(0)
W.addConst(wScalar)

sim_time = []
guide_time = []

#main loop
for t in range(numFrames):
#       densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        start = time.clock()
        if t<300:
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)

        density.save('%sdensity_%04d.vol' % (mitsubaoutdir, t))
        
        advectSemiLagrange_p(flags=flags, vel=vel, grid=density, order=1, orderSpace=2) 
        advectSemiLagrange(flags=flags, vel=vel, grid=vel,     order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags,real=density) 
        
        setWallBcs(flags=flags, vel=vel)
        addBuoyancy(density=density, vel=vel, gravity=vec3(0,-1e-3*factor,0), flags=flags)
        #addBuoyancy(density=density, vel=vel, gravity=vec3(0,-1e-2,0), flags=flags)
        
        velIn.load( input_uni % (t) )
        interpolateMACGrid( source=velIn, target=velT )
        velT.multConst(vec3(factor))

        start_g = time.clock()
        PD_fluid_guiding(vel=vel, velT=velT, flags=flags, weight=W, blurRadius=beta, pressure=pressure, \
                tau = tau, sigma = sigma, theta = theta, preconditioner = PcMGStatic, zeroPressureFixing=True )
        end_g = time.clock()

        setWallBcs(flags=flags, vel=vel)
        if 0:
                projectPpmFull( density, output_fall20_ppm % (t) , 0, 2.0 );
                density.save(output_fall20_uni % (t))
        
        s2.step()
        end = time.clock()

        sim_time.append((end-start))


ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


print('[resx, resy, resz, w beta: tg ts ]',res2,res2,res2, wScalar, beta , ts)
file = open(output_runtime+"/meanTimes_PD_"+str(res2)+"_"+str(wScalar)+"_"+str(beta)+".txt","w")
file.write("resx, resy, resz, W beta: tg & ts "+str(res2)+", "+str(res2)+", "+str(res2)+", "+ str(wScalar)+", "+str(beta)+": "+str(ts))
file.close()
