#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args <= 5:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

cutoffD= float(sys.argv[4])
scale= int(sys.argv[5])


# - - - - - - - - - - - - - - - - #
# Different number of arguments!! #
# Try 32 32 32 2 3                #
# - - - - - - - - - - - - - - - - #


useReflectionMethod= False

if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0


timings = Timings()
#sim_time = []


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x * scale
res_highr_y = res_lowr_y * scale
res_highr_z = res_lowr_z * scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)



s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()



# prepare grids
flags = s.create(FlagGrid)
vel_lowr = s.create(MACGrid)
vel_highr = s.create(MACGrid)
vel_guided = s.create(MACGrid)
vel_guided_v2 = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

#vel_lowr.setConst(vec3(0))

# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

# setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()

#gui.pause()

# org submission plume
#source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.14, z=gs_highr * vec3(0, 0.02, 0))

# source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.09, z=gs_highr * vec3(0, 0.02, 0))

# buoyancy = vec3(0, 1e-3, 0)


sim_time = []
guide_time = []





applySimpleNoiseVec3(flags, vel_lowr, noise)

# applySimpleNoiseVec3(flags, vel_highr, noise)

vel_guided.copyFrom(vel_highr)
ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_guided, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)

vel_guided_v2.copyFrom(vel_highr)
ideal_filtering_smoke_guiding_v2(vel_lowres=vel_lowr,vel_highres=vel_guided_v2, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)

gui.pause()
