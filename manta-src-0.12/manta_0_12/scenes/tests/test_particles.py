#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time

res = 32
gs = vec3(res, res, res)

s = Solver(name='sim', gridSize = gs, dim=3)
s.timestep =1.0
timings = Timings()

# particle sampling settings
discretization = 3
randomness = 0.5


# prepare grids
flags = s.create(FlagGrid)
vel = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()

# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(45)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2

source = s.create(Cylinder, center=gs * vec3(0.5, 0.1, 0.5), radius=res * 0.14, z=gs * vec3(0, 0.02, 0))

densityInflow(flags=flags, density=density_change, noise=noise, shape=source, scale=1, sigma=0.5)

sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)

particlesToDensity(flags, density, pp, discretization=discretization)

gui.pause()

vel.setConst(vec3(0,0.5,0))

def densityInflow_particles(flags, density, noise, shape, scale, sigma, pp):
    density_change.copyFrom(density)
    densityInflow(flags=flags, density=density_change, noise=noise, shape=source, scale=1, sigma=0.5)
    density_change.sub(density)
    sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
    particlesToDensity(flags, density, pp, discretization=discretization)

def advectDensity_particles(flags, vel, grid, pp):
    pp.advectInGrid(flags=flags, vel=vel, integrationMode=IntRK4, deleteInObstacle=True)
    particlesToDensity(flags, grid, pp, discretization=discretization)

while true:
    # source.applyToGrid(grid=density, value=1)
    densityInflow_particles(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5, pp=pp)
    advectDensity_particles(flags=flags, vel=vel, grid=density, pp=pp)
    resetOutflow(flags=flags, real=density)
    s.step()
