#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args <= 5:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

cutoffD= float(sys.argv[4])
num_frames = int(sys.argv[5])


# - - - - - - - - - - - - - - - - - #
# Different number of arguments!!   #
# Try 64 64 64 8 200                #
# - - - - - - - - - - - - - - - - - #


useReflectionMethod= False

if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0


timings = Timings()
#sim_time = []
guide_time = [None]*num_frames
guidePress_time = [None]*num_frames


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x
res_highr_y = res_lowr_y
res_highr_z = res_lowr_z

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)



s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()



# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_p = s.create(MACGrid)
lowpass = s.create(MACGrid)
lowpass_p = s.create(MACGrid)
highpass = s.create(MACGrid)
highpass_p = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

#vel_lowr.setConst(vec3(0))

# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

# setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()

#gui.pause()

# org submission plume
#source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.14, z=gs_highr * vec3(0, 0.02, 0))

# source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.09, z=gs_highr * vec3(0, 0.02, 0))

# buoyancy = vec3(0, 1e-3, 0)


sim_time = []
guide_time = []





applySimpleNoiseVec3(flags, vel_highr, noise)
setWallBcs(flags=flags, vel=vel_highr)
solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

vel_highr_p.copyFrom(vel_highr)
setWallBcs(flags=flags, vel=vel_highr_p)
solvePressure(flags=flags, vel=vel_highr_p, pressure=pressure)

highpass.copyFrom(vel_highr)
ideal_filtering_smoke_guiding_v2(vel_lowres=vel_lowr,vel_highres=highpass, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
# gaussian_filter_smoke_guiding(vel_lowres=vel_lowr,vel_highres=highpass, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
lowpass.copyFrom(vel_highr)
lowpass.multConst(-1.*Vec3(1,1,1))
lowpass.add(highpass)
lowpass.multConst(-1.*Vec3(1,1,1))

lowpass_p.copyFrom(lowpass)
setWallBcs(flags=flags, vel=lowpass_p)
solvePressure(flags=flags, vel=lowpass_p, pressure=pressure)

highpass_p.copyFrom(highpass)
setWallBcs(flags=flags, vel=highpass_p)
solvePressure(flags=flags, vel=highpass_p, pressure=pressure)

gui.pause()
