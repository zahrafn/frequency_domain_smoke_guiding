#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args < 5:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

num_frames = int(sys.argv[4])
scale = int(sys.argv[5])

cutoff=1
gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)


outputDir = "../../../mantaflow_output_fall19/laplac/"
folderName_div = outputDir + "div/";
s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0

# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
div_grid = s.create(RealGrid)


bWidth= 0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()


if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()



if not os.path.exists(folderName_div):
    os.makedirs(folderName_div)

laplacian_eigenfluid(vel_highr,gs_highr, 1, 1, 1, 1, bWidth)

#main loop
for t in range(num_frames):
    #ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
    divRealGrid(div=div_grid ,grid=vel_highr)
    writeRealGrid(folderName_div, div_grid, gs_highr, t)

     
    s.step()
   
   
