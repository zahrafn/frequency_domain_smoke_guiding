#
# Simulation of a 3D buoyant smoke density plume at low resolution,
# to be used as guiding velocities for the high resolution version
#

from manta import *

from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args < 3:
    print("incorrect number of arguments")
    sys.exit()

res0= int(sys.argv[1])
scale = int(sys.argv[2])
numFrames = int(sys.argv[3])

# solver params
#res0 = 70
#scale = 1.0
res = res0*scale
gs = vec3(res,(2*res),res)
#gs = vec3(res,res,res)

s = Solver(name='main', gridSize = gs, dim=3)
#s.timestep = 0.65*scale
s.timestep = 1.0 *scale
#numFrames = 400
timings = Timings()

mitsubaoutdir = "../mitsuba/output_fall20/plume/3d01_low/"

output_fall20_uni = mitsubaoutdir+'plume3DLowRes_%04d.uni'
# output_fall20_ppm = mitsubaoutdir+'/plume3DLowRes_%04d.ppm'

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)

if not os.path.exists(output_fall20_uni):
    os.makedirs(output_fall20_uni)



# prepare grids
flags = s.create(FlagGrid)
vel = s.create(MACGrid)
velT = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)


#### Density tracking with particles ####

# Particle sampling settings
useParticles = False
discretization = 3
randomness = 0.5
# Data structures
density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)


# noise field
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
#noise.valScale = 1
noise.valOffset = 0.75
noise.timeAnim = 0.5



bWidth=0
flags.initDomain(boundaryWidth=bWidth) 
flags.fillGrid()
setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow|FlagEmpty) 

if (GUI):
	gui = Gui()
	gui.show()

source = s.create(Cylinder, center=gs*vec3(0.5,0.04,0.5), radius=res*0.17, z=gs*vec3(0, 0.02, 0))
	
#main loop
for t in range(int(numFrames*scale)):
	if t<300:
		densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)

	density.save('%sdensity_%04d.vol' % (mitsubaoutdir, t))
		
	advectSemiLagrange_p(flags=flags, vel=vel, grid=density, order=1, orderSpace=2)    
	advectSemiLagrange(flags=flags, vel=vel, grid=vel,     order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
	resetOutflow(flags=flags,real=density) 
	
	setWallBcs(flags=flags, vel=vel)
	addBuoyancy(density=density, vel=vel, gravity=vec3(0,-1e-3,0), flags=flags)
	#addBuoyancy(density=density, vel=vel, gravity=vec3(0,-1e-2,0), flags=flags)
	
	solvePressure(flags=flags, vel=vel, pressure=pressure)

	setWallBcs(flags=flags, vel=vel)
	#projectPpmFull( density, output_fall20_ppm % (t+1) , 0, 2.0 );
	vel.save( output_fall20_uni % (t) )
	
	s.step()

