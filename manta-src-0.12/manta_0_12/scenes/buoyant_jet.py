from manta import *
import os
from timeit import default_timer as timer
import sys
import time

args = len(sys.argv)
if args < 8:
        print("incorrect number of arguments")
        sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])

cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])
scale = int(sys.argv[8])

useGauss = int(sys.argv[9])
useParticles  = int(sys.argv[10]) 
print("******* useGauss, useParticles:", useGauss,useParticles)
if cutoffD!=0:
        cutoff = 1./cutoffD
else:
        cutoff = 0

gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)

useReflectionMethod = True
output_spring20Dir = "../../../mantaflow_output_spring20/buoyant_jet/"
guide_vel_dir = output_spring20Dir + "lowResVelocity3D/";


mitsubaoutdir = "../mitsuba/output_spring20/buoyant_jet/buoyant_jet_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(int(cutoffD))+"_"+str(useGauss)+str(useParticles)


output_spring20_obstPos = mitsubaoutdir+'/obstPos.txt'
output_runtime = "../mitsuba/output_spring20/runtime_data"


s = Solver(name='lowRes', gridSize = gs_highr, dim=3)



if not os.path.exists(guide_vel_dir):
        os.makedirs(guide_vel_dir)
        os.makedirs(guide_vel_dir+'vx')
        os.makedirs(guide_vel_dir+'vy')
        os.makedirs(guide_vel_dir+'vz')



if not os.path.exists(mitsubaoutdir ):
        os.makedirs(mitsubaoutdir )



s.timestep = 1.0
timings = Timings()

# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
obsVel  = s.create(MACGrid)


#### Density tracking with particles ####

# Particle sampling settings
#useParticles = True
discretization = 3
randomness = 0.5
# Data structures
density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
#        print("useGrid")
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
#        print("useParticle")
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
#        print("useGrid")
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
#        print("useParticle")
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)



# noise field, tweak a bit for smoke source
noise1 = s.create(NoiseField, loadFromFile=True)
noise1.posScale = vec3(45)
noise1.clamp = True
noise1.clampNeg = 1
noise1.clampPos = 1
noise1.valOffset = 0.75
noise1.timeAnim = 0.2

# noise field, tweak a bit for smoke source
noise2 = s.create(NoiseField, loadFromFile=True)
noise2.posScale = vec3(15)
noise2.clamp = True
noise2.clampNeg = 0.5
noise2.clampPos = 1
noise2.valOffset = 0.75
noise2.timeAnim = 1.0




bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth, 'xXyYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show( True )
    # gui.nextVec3Display()

# gui.pause()

if not os.path.exists(mitsubaoutdir):
        os.makedirs(mitsubaoutdir)

def pulse(t, start, end):
        if t < start or t > end:
                return 0.
        return (4.*(t - start)*(end - t)/(end - start)**2)**3

source = s.create(Cylinder, center=gs_highr* vec3(0.1, 0.1, 0.5), radius=(res_highr_x)* 0.04, z=gs_highr* vec3(0.03, 0, 0))


buoyancy = vec3(0, 1e-3, 0)
velInflow = vec3(1.5e-2, 0, 0)
sim_time=[]

#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        velocityInflow(flags=flags, vel=vel_highr, velToSet=velInflow * float(res_highr_x), noise=noise2, shape=source, sigma=0.5) 
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth,orderSpace=1)

        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)

    #setWallBcs(flags=flags, vel=vel_highr)
    #addBuoyancy(density=density, vel=vel_highr, gravity=vec3(0.0,-1e-4,0.0), flags=flags)

        # divRealGrid(div=div_before_press ,grid=vel_highr)
        # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)
    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        velocityInflow(flags=flags, vel=vel_highr, velToSet=velInflow * float(res_highr_x), noise=noise2, shape=source, sigma=0.5)
        #advection

        #advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2)
        #advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth)

        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        velocityInflow(flags=flags, vel=vel_highr, velToSet=velInflow * float(res_highr_x), noise=noise2, shape=source, sigma=0.5)

        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        #advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2)
        #advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth)        
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

    # divRealGrid(div=div_before_press ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)

    #if guide==0:
    #    setWallBcs(flags=flags, vel=vel_highr)
    #    solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t

#main loop
t = 0
while t < num_frames:
        mantaMsg('\nFrame %i' % (s.frame))
        start = time.clock()
        t = step(t)
        #timings.display()

        if unguide_lowres:
                writeOutMACGrid(folderName=guide_vel_dir, vel=vel_highr,res = gs_highr,frameNum=t,binary=False)
        elif guide:
                
                readInMACGrid(folderName=guide_vel_dir, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
                if useGauss:
 #                       print("useGauss")
                        gaussian_filter_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)                    
                else:   
  #                      print("useIdeal")
                        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        
        setWallBcs(flags=flags, vel=vel_highr)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

        end = time.clock()
        sim_time.append((end-start))

ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


print('[resx resy resz  cutoff tg ts ]',res_highr_x,res_highr_y,res_highr_z, cutoff, ts)
#file = open(output_runtime+"/meanTimes_buoyantJet_"+str(res_highr_x)+"_"+str(cutoff)+".txt","w")
#file.write("resx, resy, resz, cutoff: ts "+str(res_highr_x)+", "+str(res_highr_y)+", "+str(res_highr_z)+", "+ str(cutoff)+": "+str(ts))
#file.close()

#fob = open("%s/obstSize.txt" % mitsubaoutdir,'w')
#fob.write("%5.4f" % (obsSize))
