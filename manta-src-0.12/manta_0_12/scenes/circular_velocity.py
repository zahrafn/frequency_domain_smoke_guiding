#
# Simple example scene for a 2D circularly guided simulation
# Main params:
#       W = guiding weight, different strengths for top / bottom halves
#       beta = blur radius
#       scale = up-res factor
#
from manta import *
import math
import os
from timeit import default_timer as timer
import time
import sys
import struct


args = len(sys.argv)
if args < 4:
    print("incorrect number of arguments")
    sys.exit()

res0= int(sys.argv[1])
scale = int(sys.argv[2])
cutoff= float(sys.argv[3])
num_frames = int(sys.argv[4])


# solver params
# res0 = 128
# scale =1
# num_frames = 200
res = res0*scale

# cutoff = 1/16.
gs = vec3(res,res,1)
s = Solver(name='main', gridSize = gs, dim=2)
s.timestep = 2.0/scale
#s.timestep = 2.0/scale

timings = Timings()
#sim_time = [None]*(num_frames-1)
#guide_time = [None]*(num_frames-1)
#rest_time = [None]*(num_frames-1)
#guidePress_time = [None]*(num_frames-1)

sim_time = []
guide_time = []
rest_time = []
guidePress_time = []



output_ppm = '../../../mantaflow_output/circularVelocity/guided_'+str(res)+"_cutoff_"+str(cutoff)+'/ppm/cv_%04d.ppm'
output_png = '../../../mantaflow_output/circularVelocity/guided'+str(res)+"_cutoff_"+str(cutoff)+'/png/cv_%04d.png'
#
# outputDir_ppm = '../../../mantaflow_output/circularVelocity/highres128/ppm/cv_%04d.ppm'
# outputDir_png = '../../../mantaflow_output/circularVelocity/highres128/png/cv_%04d.png'

tau = 1.0
sigma = 0.99/tau
theta = 1.0

imageDir1 = os.path.dirname(output_ppm)
if not os.path.exists(imageDir1):
    os.makedirs(imageDir1)

imageDir2 = os.path.dirname(output_png)
if not os.path.exists(imageDir2):
    os.makedirs(imageDir2)

# prepare grids
flags = s.create(FlagGrid)
vel = s.create(MACGrid)
velT = s.create(MACGrid)
velT_start = s.create(MACGrid)
velT_advector = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

if (GUI):
    gui = Gui()
    gui.show()

#gui.pause()

source = s.create(Cylinder, center=gs*vec3(0.5,0.2,0.5), radius=gs.y*0.14, z=gs*vec3(0, 0.02*1.5, 0))
getSpiralVelocity2D(flags=flags, vel=velT, strength=.5)
getSpiralVelocity2D(flags=flags, vel=vel, strength=.5)

useReflectionMethod = False

# setGradientYWeight(W=W, minY=0,     maxY=res/2, valAtMin=valAtMin, valAtMax=valAtMin)
# setGradientYWeight(W=W, minY=res/2, maxY=res,   valAtMin=valAtMax, valAtMax=valAtMax)

buoyancy = vec3(0, -0.5e-3, 0) #1e-3

#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<120:
            # source.applyToGrid(grid=density, value=1)
            source.applyToGrid(grid=density, value=1.5)
        #densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        #density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        advectSemiLagrange(flags=flags, vel=velT, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=velT, grid=velT, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)
        addBuoyancy(density=density, vel=velT, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=velT)

    #setWallBcs(flags=flags, vel=velT)
    #addBuoyancy(density=density, vel=velT, gravity=vec3(0.0,-1e-4,0.0), flags=flags)

        # divRealGrid(div=div_before_press ,grid=velT)
        # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)
    else:
        velT_start.copyFrom(velT)
        velT_advector.copyFrom(velT)
        # source
        if t<120:
            # source.applyToGrid(grid=density, value=1)
            source.applyToGrid(grid=density, value=1.5)
            #densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        #density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # advection
        advectSemiLagrange(flags=flags, vel=velT_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=velT_advector, grid=velT, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=velT)
        addBuoyancy(density=density, vel=velT, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        velT_advector.copyFrom(velT)
        solvePressure(flags=flags, vel=velT_advector, pressure=pressure)
        velT.multConst(vec3(-1.))
        velT.addScaled(velT_advector, vec3(2.))
        velT_advector.multConst(vec3(2.))
        velT_advector.addScaled(velT_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<120:
            # source.applyToGrid(grid=density, value=1)
            source.applyToGrid(grid=density, value=1.5)
            #densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        #density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # external forces
        setWallBcs(flags=flags, vel=velT)
        addBuoyancy(density=density, vel=velT, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange(flags=flags, vel=velT_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=velT_advector, grid=velT, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

    # divRealGrid(div=div_before_press ,grid=velT)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)

    #if guide==0:
    #    solvePressure(flags=flags, vel=velT, pressure=pressure)
    s.step()
    t = t + 1
    return t



#main loop

for t in range(200):
    start = time.clock()
    # mantaMsg('\nFrame %i' % (s.frame))
    getSpiralVelocity2D(flags=flags, vel=velT, strength=.5)

    resetOutflow(flags=flags,real=density)

#    t = step(t)


    if t<120:
        source.applyToGrid(grid=density, value=1.5)

    advectSemiLagrange(flags=flags, vel=vel, grid=density, order=2)
    advectSemiLagrange(flags=flags, vel=vel, grid=vel,     order=2)

    setWallBcs(flags=flags, vel=vel)
    addBuoyancy(density=density, vel=vel, gravity=buoyancy, flags=flags)


    start_g = time.clock()
   # ideal_filtering_smoke_guiding(vel_lowres=velT,vel_highres=vel, lowres=gs,highres=gs,cutoff=cutoff, bw = bWidth)
    ideal_filtering_smoke_guiding_v2(vel_lowres=velT,vel_highres=vel, lowres=gs,highres=gs,cutoff=cutoff, bw = bWidth)
    end_g = time.clock()
#    print("t: ",t)

    #guide_time[t] = (end_g-start_g)*1000.

    setWallBcs(flags=flags, vel=vel)
    solvePressure(flags=flags, vel=vel, pressure=pressure)
    end_pg = time.clock()
    
    s.step()

    
    end = time.clock()
#    if t>1 and t< 200:
#        guide_time[t] = (end_g-start_g)*1000.
#        guidePress_time[t] = (end_pg-start_g)*1000.
#        sim_time[t] = (end-start)*1000.
 
# # rest_time[t] = ((end - start) - (end_g - start_g))*1000.
        
    guide_time.append((end_g-start_g)*1000.)
    guidePress_time.append((end_pg-start_g)*1000.)
    sim_time.append((end-start)*1000.)


    if 1:
        projectPpmFull( density, output_ppm % t , 0, 1.0 );
        # gui.screenshot(output_png % t)




# tr = float("%.2f"% (sum(rest_time) / float(len(rest_time))))
tg = float("%.2f"% (sum(guide_time) / float(len(guide_time))))
tpg = float("%.2f"% (sum(guidePress_time) / float(len(guidePress_time))))
ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


print('[res cutoffts tg tpg ts ]',res, cutoff, tg, tpg, ts)
#file = open("runTime/meanTimes_circularVel_IdealLHGuiding"+str(cutoff)+".txt","w") 
#file.write("res, cutoff: tg & tgp & ts "+str(res)+", "+ str(cutoff)+": "+ str(tg)+ " & "+ str(tpg)+" & "+str(ts)) 
#file.close() 

