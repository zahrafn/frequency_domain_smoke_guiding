#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args < 8:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
upsample = int(sys.argv[5])
gaussfilter = int(sys.argv[6])
guide = int(sys.argv[7])


cutoffD= float(sys.argv[8])
num_frames = int(sys.argv[9])

scale = int(sys.argv[10])


print(res_lowr_x , res_lowr_y, res_lowr_z, unguide_lowres, guide, cutoffD, num_frames)
useReflectionMethod= False

if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0


timings = Timings()
#sim_time = []
guide_time = [None]*num_frames
guidePress_time = [None]*num_frames


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)


outputDir = "../../../mantaflow_output/plume3D/"
guide_vel = outputDir + "lowResVelocity3D/";

mitsubaoutdir = "../mitsuba/output_fall20/plume/plume_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))

if gaussfilter:
        mitsubaoutdir = "../mitsuba/output_fall20/plume/plume_gauss_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))

output_runtime = "../mitsuba/output_fall20/runtime_data"


s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()



# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
vel_highr_lowPassed = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

#vel_lowr.setConst(vec3(0))



#### Density tracking with particles ####

# Particle sampling settings
useParticles = True
discretization = 3
randomness = 0.5
# Data structures
density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)




# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.5

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()
setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)
if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()

#gui.pause()

if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')
        os.makedirs(guide_vel+'vz')

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)
# org submission plume
source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.04, 0.5), radius=res_highr_x * 0.17, z=gs_highr * vec3(0, 0.02, 0))


buoyancy = vec3(0, 1e-3, 0)


sim_time = []
guide_time = []
t_adv_vel = []
t_adv_den = []
t_press = []
t_others = []
t_guide = []
t1=t2=t3=t4=t5=t6=t1s=t1e=t2e=t3e=t4s=t4e=t5s=t5e=t6e = 0

#### Simulation step ####

def step(t):
    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        density.save('%s/density_high_%04d.uni' % (mitsubaoutdir, t))
        if unguide_lowres:
                density.save('%s/density_low_%04d.uni' % (mitsubaoutdir, t))
                vel_highr.save('%s/velocity_low_%04d.uni' % (mitsubaoutdir, t))
        t1s = time.clock()
        advectSemiLagrange_p(flags=flags, vel=vel_highr, grid=density, order=1, orderSpace=2)
        t1e = time.clock()
        t1 = t1e - t1s
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        t2e = time.clock()
        t2 = t2e - t1e

        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)
        t3e = time.clock()
        t3 = t3e - t2e
    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        if unguide_lowres:
                density.save('%s/density_%04d.uni' % (mitsubaoutdir, t))
        # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        if unguide_lowres:
                density.save('%s/density_%04d.uni' % (mitsubaoutdir, t))
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

    if guide==0:
       setWallBcs(flags=flags, vel=vel_highr)
       solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    
    s.step()
    t = t + 1
    return t, t1, t2 ,t3


#### main loop ####
t = 0
while t < num_frames:
    mantaMsg('\n################## Frame %i' % (s.frame))

    start = time.clock()
    t,t1,t2,t3 = step(t)
        
    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t,binary=False)
        
    elif upsample:

        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        upsampling_smoke(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr, bw= bWidth)
        setWallBcs(flags=flags, vel=vel_highr)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

    elif guide or gaussfilter:

        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        start_g = time.clock()
        if guide:
                t4s = time.clock()
                ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
                t4e = time.clock()
                t4 = t4e - t4s
        else:
                gaussian_filter_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        end_g = time.clock() 
        t5s = time.clock()
        setWallBcs(flags=flags, vel=vel_highr)
        t5e = time.clock()
        t5 = t5e - t5s
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
        t6e = time.clock()
        t6 = t6e - t5e

#        t_adv_vel.append((t2)*1000.)
#        t_adv_den.append((t1)*1000.)
#        t_press.append((t6)*1000.)
#        t_guide.append((t4)*1000.)
#        t_others.append((t3+t5)*1000.)
#
#        guide_time.append((end_g-start_g)*1000.)
#
#    end = time.clock()
#    sim_time.append(end-start)
#    if(not upsample):
#        ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))
#        t_adv_velm = float("%.2f"% (sum(t_adv_vel) / float(len(t_adv_vel))))
#        t_adv_denm = float("%.2f"% (sum(t_adv_den) / float(len(t_adv_den))))
#        t_pressm = float("%.2f"% (sum(t_press) / float(len(t_press))))
#        t_guidem = float("%.2f"% (sum(t_guide) / float(len(t_guide))))
#        t_othersm = float("%.2f"% (sum(t_others) / float(len(t_others))))

#        print('[resx resy resz  cutoff tg ts ]',res_highr_x,res_highr_y,res_highr_z, cutoff, ts)
#        file = open(output_runtime+"/meanTimes_idealGuid_"+str(res_highr_x)+"_"+str(cutoff)+".txt","w")
#        file.write("resx, resy, resz, cutoff: tg & ts "+str(res_highr_x)+", "+str(res_highr_y)+", "+str(res_highr_z)+", "+ str(cutoff)+": "+str(ts))
#        file.close()
#
#        print('t_adv_velm , t_adv_denm, t_pressm, t_guidem, t_othersm', t_adv_velm , t_adv_denm, t_pressm , t_guidem, t_othersm)
