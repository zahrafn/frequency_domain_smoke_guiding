
#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys

# to generate the low-res guide velocities      scale = 1 guideGenerator = True
# to generate the high-res unguided             scale = 2 guideGenerator = True
# to generate the high-res guided               scale = 2 guideGenerator = False ->set the cutoff

#unguide_lowres= True # make sure to be False for guiding
#guide =  False
#num_frames = 200
#res_lowr_x = 100
#res_lowr_y = 120
#scale =2;
#cutoff = 1/8.

#if unguide_lowres:
#    scale =1
#else:
#    scale = 2



args = len(sys.argv)
if args < 7:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])

unguide_lowres = int(sys.argv[3])
guide = int(sys.argv[4])

cutoffD= float(sys.argv[5])
num_frames = int(sys.argv[6])

scale = int(sys.argv[7])
if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0



outputDir = "../../../mantaflow_output_fall19/plume2D/"
guide_vel = outputDir + "lowResVelocity2D/";
# output_ppm = outputDir +'highres200X240/ppm/pl_%04d.ppm'
# output_png = outputDir +'highres200X240/png/pl_%04d.png'

# output_ppm = outputDir +'lowres100X120/ppm/pl_%04d.ppm'
# output_png = outputDir +'lowres100X120/png/pl_%04d.png'
#
output_ppm = outputDir +'guided200X240_cutoff8/ppm/pl_%04d.ppm'
output_png = outputDir +"plume2d_"+str(res_lowr_x*scale)+"X"+str(res_lowr_y*scale)+"_cutoff"+str(int(cutoffD))+"/png/pl_%04d.png"



# folderName_div_before_guid = outputDir + "div/before_guid/";
# folderName_div_after_guid = outputDir + "div/after_guid/";
# folderName_div_before_press = outputDir + "div/before_press/";
# folderName_div_after_guid_press = outputDir + "div/after_guid_press/";


gs_lowr = vec3(res_lowr_x,res_lowr_y,1)

res_highr_x = res_lowr_x * scale
res_highr_y = res_lowr_y * scale

gs_highr = vec3(res_highr_x,res_highr_y,1)
s = Solver(name='highRes', gridSize = gs_highr, dim=2)
s.timestep =1.0
timings = Timings()


if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')

imageDir1 = os.path.dirname(output_ppm)
imageDir2 = os.path.dirname(output_png)
if not os.path.exists(imageDir1):
    os.makedirs(imageDir1)
if not os.path.exists(imageDir2):
    os.makedirs(imageDir2)


# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_lowPassed = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

div_before_guiding = s.create(RealGrid)
div_after_guiding = s.create(RealGrid)
div_before_press = s.create(RealGrid)
div_after_guiding_press=s.create(RealGrid)


# noise field, tweak a bit for smoke source
# noise = s_highr.create(NoiseField, loadFromFile=True)
# noise.posScale = vec3(45)
# noise.clamp = True
# noise.clampNeg = 0
# noise.clampPos = 1
# noise.valOffset = 0.75
# noise.timeAnim = 0.2

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

# setOpenBound(flags_highr, bWidth,'xXyYzZ',FlagOutflow|FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()
    gui.nextVec3Display()
    gui.nextVec3Display()
#gui.pause()


source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.1, z=gs_highr * vec3(0, 0.02, 0))


#main loop
for t in range(num_frames):
    mantaMsg('\n################## Frame %i' % (s.frame))

    start = timer()

    if t<150:
        source.applyToGrid(grid=density, value=1)
        # densityInflow(flags=flags_highr, density=density_highr, noise=noise, shape=source, scale=1, sigma=0.5)


    advectSemiLagrange(flags=flags, vel=vel_highr, grid=density, order=2, orderSpace=2, orderTrace=2)
    advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
    resetOutflow(flags=flags, real=density)

    setWallBcs(flags=flags, vel=vel_highr)
    addBuoyancy(density=density, vel=vel_highr, gravity=vec3(0, -5e-3, 0), flags=flags)

    # divRealGrid(div=div_before_press ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)


    # readInMACGrid(folderName=folderName, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=True)
    # ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)

    solvePressure(flags=flags, vel=vel_highr, pressure=pressure)


    s.step()
    #timings.display()


    # divRealGrid(div=div_before_guiding ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_guid, div_before_guiding, res_highr,t)

    # mantaMsg('\n End of Frame %i' % (s_highr.frame))

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t,binary=True)
    elif guide:
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=True)
        for itr in range(50):
            ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)
            solvePressure(flags=flags, vel=vel_highr, pressure=pressure)



    if 1:# and t%scale==0:
        projectPpmFull(density, output_ppm % t, 0, 1.0);
        gui.screenshot(output_png % t);
