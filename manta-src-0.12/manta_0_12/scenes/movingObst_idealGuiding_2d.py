#
# Simple example scene with a moving obstacle
#
from manta import *
import os

unguide_lowres= False # make sure to be False for guiding
guide =  True
num_frames = 400
res_lowr_x = 100
res_lowr_y = 100
scale =1;
cutoff = 1/4.

if unguide_lowres:
    scale =1
else:
    scale = 2

outputDir = "../../../mantaflow_output/plume2D_movingObst/"
guide_vel = outputDir + "lowResVelocity2D/";

# output_ppm = outputDir +'guided64X64_cutoff4/ppm/pl_%04d.ppm'
# output_png = outputDir +'guided64X64_cutoff4/png/pl_%04d.png'

output_ppm = outputDir +'highres64X64/ppm/pl_%04d.ppm'
output_png = outputDir +'highres64X64/png/pl_%04d.png'

if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')

imageDir1 = os.path.dirname(output_ppm)
imageDir2 = os.path.dirname(output_png)
if not os.path.exists(imageDir1):
    os.makedirs(imageDir1)
if not os.path.exists(imageDir2):
    os.makedirs(imageDir2)


gs_lowr = vec3(res_lowr_x,res_lowr_y,1)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale

gs_highr = vec3(res_highr_x,res_highr_y,1)
s= Solver(name='lowRes', gridSize = gs_highr, dim=2)

# allocate grids
flags   = s.create(FlagGrid)
vel     = s.create(MACGrid)
vel_lowr     = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
obsVel  = s.create(MACGrid)

bWidth = 1
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()
setOpenBound(flags, bWidth,'yY',FlagOutflow|FlagEmpty)

source = Box( parent=s, p0=gs_lowr*scale*vec3(0.30,0.1,0.1), p1=gs_lowr*scale*vec3(0.50,0.9,0.9))
source.applyToGrid(grid=density, value=1)

# init obstacle properties
obsPos = vec3(0.2,0.4,0.5)
obsVelVec = vec3(0.6,0.2,0.0) * (1./100.) * float(res_lowr_y*scale) # velocity in grid units for 100 steps
obsSize = 0.1
obsVel.setConst(obsVelVec)
obsVel.setBound(value=Vec3(0.), boundaryWidth=bWidth+1) # make sure walls are static
obs = "dummy"; phiObs = "dummy2"

if (GUI):
    gui = Gui()
    gui.show( True )
#gui.pause()

#main loop
for t in range(num_frames):
    mantaMsg('\nFrame %i' % (s.frame))

    advectSemiLagrange(flags=flags, vel=vel, grid=density, order=2)
    advectSemiLagrange(flags=flags, vel=vel, grid=vel,     order=2, openBounds=True, boundaryWidth=bWidth)
    resetOutflow(flags=flags,real=density)

    if t<=100:
        flags.initDomain(boundaryWidth=bWidth)
        flags.fillGrid()
        setOpenBound(flags, bWidth,'yY',FlagOutflow|FlagEmpty)

        del obs, phiObs
        # use sphere or box?
        if 1:
            obs = Sphere( parent=s, center=gs_lowr*scale*obsPos + float(t) * obsVelVec, radius=res_lowr_y*scale*obsSize)
        else:
            obs = Box( parent=s, p0=gs_lowr*scale*vec3(0.15-obsSize*0.5,0.2,0.4) + float(t) * obsVelVec, \
                       p1=gs_lowr*scale*vec3(0.15+obsSize*0.5,0.5,0.6) + float(t) * obsVelVec)
        phiObs = obs.computeLevelset()

        setObstacleFlags(flags=flags, phiObs=phiObs)
        flags.fillGrid()

        obs.applyToGrid(grid=density, value=0.) # clear smoke inside
    elif t==101:
        # stop moving
        obsVel.setConst(Vec3(0.))

    setWallBcs(flags=flags, vel=vel, phiObs=phiObs, obvel=obsVel)
    solvePressure(flags=flags, vel=vel, pressure=pressure)

    s.step()

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel,res = gs_highr,frameNum=t,binary=True)
    elif guide:
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=True)
        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff)
        solvePressure(flags=flags, vel=vel, pressure=pressure)

    if 0:# and t%scale==0:
        projectPpmFull(density, output_ppm % t, 0, 1.0);
        gui.screenshot(output_png % t);