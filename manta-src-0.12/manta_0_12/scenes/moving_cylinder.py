#
#
from manta import *
import os
import sys
import math
import time

args = len(sys.argv)
if args < 8:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])

cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])
scale = int(sys.argv[8])

if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0

# unguide_lowres= 0 # make sure to be False for guiding
# guide =  1
# num_frames = 200

# res_lowr_x = 32
# res_lowr_y = 64
# res_lowr_z = 32
# scale =1;
# cutoff = 1/4.


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)

output_spring20Dir = "../../../mantaflow_output_spring20/moving_cylinder/"
guide_vel = output_spring20Dir + "lowResVelocity3D/";

mitsubaoutdir = "../mitsuba/output_spring20/moving_cylinder/moving_cylinder_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))

output_runtime = "../mitsuba/output_spring20/runtime_data"


if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)



s= Solver(name='lowRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()

if not os.path.exists(guide_vel):
    os.makedirs(guide_vel)
    os.makedirs(guide_vel+'vx')
    os.makedirs(guide_vel+'vy')
    os.makedirs(guide_vel+'vz')


# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
vel_highr_lowPassed = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
obsVel1  = s.create(MACGrid)
obsVel2  = s.create(MACGrid)


#### Density tracking with particles ####

# Particle sampling settings
useParticles = True
discretization = 3
randomness = 0.5
# Data structures
density_change = s.create(RealGrid)
pp = s.create(BasicParticleSystem)
# Functions
def densityInflow_p(flags, density, noise, shape, scale, sigma):
    if not useParticles:
        densityInflow(flags=flags, density=density, noise=noise, shape=shape, scale=scale, sigma=sigma)
    else:
        density_change.copyFrom(density)
        densityInflow(flags=flags, density=density_change, noise=noise, shape=shape, scale=scale, sigma=sigma)
        density_change.sub(density)
        sampleDensityWithParticles(density_change, flags, pp, discretization=discretization, randomness=randomness)
        particlesToDensity(flags, density, pp, discretization=discretization)
        density.clamp(0, 1)
def advectSemiLagrange_p(flags, vel, grid, openBounds=False, **kwargs):
    if not useParticles:
        advectSemiLagrange(flags=flags, vel=vel, grid=grid, **kwargs)
    else:
        pp.advectInGridNew(flags=flags, vel=vel, integrationMode=IntRK4, deleteOutOfBounds=True, deleteInObstacle=False, stopInObstacle=False)
        pushOutofObs(parts=pp, flags=flags, phiObs=phiObs1)
        particlesToDensity(flags, grid, pp, discretization=discretization)
        density.clamp(0, 1)




div_before_guiding = s.create(RealGrid)
div_after_guiding = s.create(RealGrid)
div_before_press = s.create(RealGrid)
div_after_guiding_press=s.create(RealGrid)

# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(15)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 1.0

bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()


# init obstacle properties
obsSize = 0.025*res_highr_x
zCylin = gs_highr * vec3(0.0,0.0, 0.15)
alpha = math.pi/25


# first obst
obsPos1 = gs_highr* vec3(0.4, 0.4, 0.5)
obsVelVec1 = vec3(0.003,0.0,0.0)*gs_highr
obsVel1.setConst(obsVelVec1)
obsVel1.setBound(value=Vec3(0.1), boundaryWidth=bWidth+1) # make sure walls are static
obs1 = None
phiObs1 = None

cntr1 = obsPos1
# if using reflection t starts from 2 so fram 0,1 get witten beforehand 
f_f0 = open("%s/obstPos_%04d.txt" % (mitsubaoutdir, 0),'w')
f_f0.write("%5.4f %5.4f %5.4f %5.4f %5.4f %5.4f" % (cntr1.x-zCylin.x,cntr1.y-zCylin.y,cntr1.z-zCylin.z,cntr1.x+zCylin.x,cntr1.y+zCylin.y,cntr1.z+zCylin.z))

f_f1 = open("%s/obstPos_%04d.txt" % (mitsubaoutdir, 1),'w')
f_f1.write("%5.4f %5.4f %5.4f %5.4f %5.4f %5.4f" % (cntr1.x-zCylin.x,cntr1.y-zCylin.y,cntr1.z-zCylin.z,cntr1.x+zCylin.x,cntr1.y+zCylin.y,cntr1.z+zCylin.z))




# # second obst
# obsPos2 = gs_highr* vec3(0.5, 0.8, 0.5)
# obsVelVec2 = vec3(-0.02,0.0,0.0)*gs_highr
# obsVel2.setConst(obsVelVec2)
# obsVel2.setBound(value=Vec3(0.1), boundaryWidth=bWidth+1) # make sure walls are static
# obs2 = "dummy"; phiObs2 = "dummy2"
# cntr2 = obsPos2


setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)


if (GUI):
    gui = Gui()
    gui.show( True )
#gui.pause()

source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.04, z=gs_highr * vec3(0, 0.02, 0))


useReflectionMethod= True
buoyancy = vec3(0, 1e-3, 0)

sim_time =[]
#### Simulation step ####

def step(t):

    global obs1, phiObs1, cntr1

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        advectSemiLagrange_p(flags=flags, vel=vel_highr, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr,phiObs=phiObs1, obvel=obsVel1)

    

    #setWallBcs(flags=flags, vel=vel_highr)
    #addBuoyancy(density=density, vel=vel_highr, gravity=vec3(0.0,-1e-4,0.0), flags=flags)

        # divRealGrid(div=div_before_press ,grid=vel_highr)
        # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)
    else:

        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        
        del obs1, phiObs1
        v = obsVelVec1 * vec3(6.0 * math.sin(alpha * t),1.0,1.0)
        cntr1 = cntr1 + v
        obs1 = Cylinder(parent=s, center=cntr1, radius=obsSize, z= zCylin)
        phiObs1 = obs1.computeLevelset()
        obsVel1.setConst(v)
        setObstacleFlags(flags=flags, phiObs=phiObs1)
        flags.fillGrid()
        obs1.applyToGrid(grid=density, value=0.) # clear smoke inside
        f1 = open("%s/obstPos_%04d.txt" % (mitsubaoutdir, t),'w')
        f1.write("%5.4f %5.4f %5.4f %5.4f %5.4f %5.4f" % (cntr1.x-zCylin.x,cntr1.y-zCylin.y,cntr1.z-zCylin.z,cntr1.x+zCylin.x,cntr1.y+zCylin.y,cntr1.z+zCylin.z))



        # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr,phiObs=phiObs1, obvel=obsVel1) 
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow_p(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        
        del obs1, phiObs1
        v = obsVelVec1 * vec3(6.0 * math.sin(alpha * t),1.0,1.0)
        cntr1 = cntr1 + v
        obs1 = Cylinder(parent=s, center=cntr1, radius=obsSize, z= zCylin)
        phiObs1 = obs1.computeLevelset()
        obsVel1.setConst(v)
        setObstacleFlags(flags=flags, phiObs=phiObs1)
        flags.fillGrid()
        obs1.applyToGrid(grid=density, value=0.) # clear smoke inside
        f1 = open("%s/obstPos_%04d.txt" % (mitsubaoutdir, t),'w')
        f1.write("%5.4f %5.4f %5.4f %5.4f %5.4f %5.4f" % (cntr1.x-zCylin.x,cntr1.y-zCylin.y,cntr1.z-zCylin.z,cntr1.x+zCylin.x,cntr1.y+zCylin.y,cntr1.z+zCylin.z))


        # external forces
        setWallBcs(flags=flags, vel=vel_highr,phiObs=phiObs1, obvel=obsVel1)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange_p(flags=flags, vel=vel_highr_advector, grid=density, order=1, orderSpace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderSpace=1)
        resetOutflow(flags=flags, real=density)

    # divRealGrid(div=div_before_press ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)

    if guide==0:
	setWallBcs(flags=flags, vel=vel_highr,phiObs=phiObs1, obvel=obsVel1)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t

#for t in range(num_frames):
t=0
ii = 0
while t<num_frames:
    mantaMsg('\n################## Frame %i' % (s.frame))
    start = time.clock()

    # moving obst   
    t = step(t)


    #s.step()

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t, binary=False)
    elif guide:
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        setWallBcs(flags=flags, vel=vel_highr,phiObs=phiObs1, obvel=obsVel1)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

    end = time.clock()
    sim_time.append((end-start))

ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


print('[resx resy resz  cutoff tg ts ]',res_highr_x,res_highr_y,res_highr_z, cutoff, ts)
#file = open(output_runtime+"/meanTimes_movingCyln_"+str(res_highr_x)+"_"+str(cutoff)+".txt","w")
#file.write("resx, resy, resz, cutoff: ts "+str(res_highr_x)+", "+str(res_highr_y)+", "+str(res_highr_z)+", "+ str(cutoff)+": "+str(ts))
#file.close()

#fob = open("%s/obstSize.txt" % mitsubaoutdir,'w')
#fob.write("%5.4f" % (obsSize))
