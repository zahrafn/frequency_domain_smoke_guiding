#
# Simple example scene for a 2D simulation with guiding
# Simulation of a buoyant smoke density plume with open boundaries at top & bottom
#
from manta import *
import os
from timeit import default_timer as timer
import sys
import time


args = len(sys.argv)
if args < 8:
    print("incorrect number of arguments")
    sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= int(sys.argv[3])

unguide_lowres = int(sys.argv[4])
guide = int(sys.argv[5])


cutoffD= float(sys.argv[6])
num_frames = int(sys.argv[7])

scale = int(sys.argv[8])
#buoyancy_x = float(sys.argv[9])
#buoyancy_y = float(sys.argv[10])
#buoyancy_z = float(sys.argv[11])
#gravity_vec= vec3(buoyancy_x,buoyancy_y, buoyancy_z ) 

useReflectionMethod = False #### Change this to use reflection method --Rahul

print(res_lowr_x , res_lowr_y, res_lowr_z, unguide_lowres, guide, cutoffD, num_frames)


if cutoffD!=0:
    cutoff = 1./cutoffD
else:
    cutoff = 0

# to generate the low-res guide velocities      unguide_lowres = True    guide = False
# to generate the high-res unguided             unguide_lowres = False   guide = False
# to generate the high-res guided               unguide_lowres = False   guide = True

# unguide_lowres= True # make sure to be False for guiding
# guide = False
# num_frames = 400
# res_lowr_x = 50
# res_lowr_y = 100
# res_lowr_z = 50
# scale =1;
# cutoff = 1/4.
timings = Timings()
sim_time = [None]*num_frames
guide_time = [None]*num_frames
guidePress_time = [None]*num_frames


gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = res_lowr_z*scale

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)


outputDir = "../../../mantaflow_output/plume3D/"
guide_vel = outputDir + "lowResVelocity3D/";

mitsubaoutdir = "../mitsuba/output_fall19/plume_3d/plume_"+str(res_highr_x)+"X"+str(res_highr_y)+"X"+str(res_highr_z)+"_cutoff_"+str(round(cutoffD))


folderName_div_before_guid = outputDir + "div/before_guid/";
folderName_div_after_guid = outputDir + "div/after_guid/";
# folderName_div_before_press = outputDir + "div/before_press/";
# folderName_div_after_guid_press = outputDir + "div/after_guid_press/";




s = Solver(name='highRes', gridSize = gs_highr, dim=3)
s.timestep =1.0
timings = Timings()



# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
vel_highr_lowPassed = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)

div_before_guiding = s.create(RealGrid)
div_after_guiding = s.create(RealGrid)
div_before_press = s.create(RealGrid)
div_after_guiding_press=s.create(RealGrid)


# noise field, tweak a bit for smoke source
noise = s.create(NoiseField, loadFromFile=True)
noise.posScale = vec3(45)
noise.clamp = True
noise.clampNeg = 0
noise.clampPos = 1
noise.valOffset = 0.75
noise.timeAnim = 0.2

bWidth=2
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth, 'xXYzZ', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show()
    gui.nextVec3Display()

#gui.pause()

if unguide_lowres:
    if not os.path.exists(guide_vel):
        os.makedirs(guide_vel)
        os.makedirs(guide_vel+'vx')
        os.makedirs(guide_vel+'vy')
        os.makedirs(guide_vel+'vz')

if not os.path.exists(mitsubaoutdir):
    os.makedirs(mitsubaoutdir)

source = s.create(Cylinder, center=gs_highr * vec3(0.5, 0.1, 0.5), radius=res_highr_x * 0.14, z=gs_highr * vec3(0, 0.02, 0))

buoyancy = vec3(0, 1e-3, 0)

#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)

    #setWallBcs(flags=flags, vel=vel_highr)
    #addBuoyancy(density=density, vel=vel_highr, gravity=vec3(0.0,-1e-4,0.0), flags=flags)

        # divRealGrid(div=div_before_press ,grid=vel_highr)
        # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)
    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise, shape=source, scale=1, sigma=0.5)
        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

    # divRealGrid(div=div_before_press ,grid=vel_highr)
    # writeRealGrid(folderName_div_before_press, div_before_press, res_highr,t)

    if guide==0:
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t


#### main loop ####
t = 0
while t < num_frames:
    mantaMsg('\n################## Frame %i' % (s.frame))

    # start = time.clock()

    t = step(t)

    #timings.display()

    if unguide_lowres:
        writeOutMACGrid(folderName=guide_vel, vel=vel_highr,res = gs_highr,frameNum=t,binary=False)

    elif guide:

        #mantaMsg('\n write div Frame %i' % (s_highr.frame))
#        divRealGrid(div=div_before_guiding ,grid=vel_highr)
#        writeRealGrid(folderName_div_before_guid, div_before_guiding, gs_highr,t)


        # start_g = time.clock()
        readInMACGrid(folderName=guide_vel, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
        ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
        # end_g = time.clock()
        setWallBcs(flags=flags, vel=vel_highr)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
        # end_pg = time.clock()



#        divRealGrid(div=div_after_guiding ,grid=vel_highr)
#        writeRealGrid(folderName_div_after_guid, div_after_guiding, gs_highr,t)

    # end = time.clock()
    # guide_time[t] = (end_g-start_g)*1000.
    # guidePress_time[t] = (end_pg-start_g)*1000.
    # sim_time[t] = (end-start)*1000.



# tr = float("%.2f"% (sum(rest_time) / float(len(rest_time))))


# tg = float("%.2f"% (sum(guide_time) / float(len(guide_time))))
# tpg = float("%.2f"% (sum(guidePress_time) / float(len(guidePress_time))))
# ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


# print('[res cutoffts tg tpg ts ]',res, cutoff, tg, tpg, ts)

# file = open("meanTimes_plume3D_IdealLHGuiding.txt"."w") 
# file.write("res, cutoff: tg & tgp & ts "+str(res)+", "+ str(cutoff)+": "+ str(tg)+ " & "+ str(tpg)+" & "+str(ts)) 
# file.close() 
