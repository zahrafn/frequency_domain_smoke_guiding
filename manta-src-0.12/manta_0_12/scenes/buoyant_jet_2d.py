from manta import *
import os
from timeit import default_timer as timer
import sys
import time

args = len(sys.argv)
if args < 8:
        print("incorrect number of arguments")
        sys.exit()

res_lowr_x= int(sys.argv[1])
res_lowr_y= int(sys.argv[2])
res_lowr_z= 1

unguide_lowres = int(sys.argv[3])
guide = int(sys.argv[4])

cutoffD= float(sys.argv[5])
num_frames = int(sys.argv[6])
scale = int(sys.argv[7])

if cutoffD!=0:
        cutoff = 1./cutoffD
else:
        cutoff = 0

gs_lowr = vec3(res_lowr_x,res_lowr_y,res_lowr_z)

res_highr_x = res_lowr_x*scale
res_highr_y = res_lowr_y*scale
res_highr_z = 1

gs_highr = vec3(res_highr_x,res_highr_y,res_highr_z)

useReflectionMethod = True
output_fall19Dir = "../../../mantaflow_output_fall19/buoyantJet2d/"
guide_vel_dir = output_fall19Dir + "lowResVelocity3D/";

output_ppm = '../../../output_spring20/buoyant_jet2d/guided'+str(res_highr_x)+"_cutoff_"+str(cutoff)+'/bj2d_%04d.ppm'
imageDir1 = os.path.dirname(output_ppm)
if not os.path.exists(imageDir1):
    os.makedirs(imageDir1)

output_runtime = "../mitsuba/output_spring20/runtime_data"


s = Solver(name='lowRes', gridSize = gs_highr, dim=2)



if not os.path.exists(guide_vel_dir):
        os.makedirs(guide_vel_dir)
        os.makedirs(guide_vel_dir+'vx')
        os.makedirs(guide_vel_dir+'vy')
        os.makedirs(guide_vel_dir+'vz')



s.timestep = 1.0
timings = Timings()

# prepare grids
flags = s.create(FlagGrid)
vel_highr = s.create(MACGrid)
vel_lowr = s.create(MACGrid)
vel_highr_start = s.create(MACGrid)
vel_highr_advector = s.create(MACGrid)
density = s.create(RealGrid)
pressure = s.create(RealGrid)
obsVel  = s.create(MACGrid)




# noise field, tweak a bit for smoke source
noise1 = s.create(NoiseField, loadFromFile=True)
noise1.posScale = vec3(45)
noise1.clamp = True
noise1.clampNeg = 1
noise1.clampPos = 1
noise1.valOffset = 0.75
noise1.timeAnim = 0.2

# noise field, tweak a bit for smoke source
noise2 = s.create(NoiseField, loadFromFile=True)
noise2.posScale = vec3(15)
noise2.clamp = True
noise2.clampNeg = 0.5
noise2.clampPos = 1
noise2.valOffset = 0.75
noise2.timeAnim = 1.0




bWidth=0
flags.initDomain(boundaryWidth=bWidth)
flags.fillGrid()

setOpenBound(flags, bWidth, 'XY', FlagOutflow | FlagEmpty)

if (GUI):
    gui = Gui()
    gui.show( True )
    # gui.nextVec3Display()

# gui.pause()


def pulse(t, start, end):
        if t < start or t > end:
                return 0.
        return (4.*(t - start)*(end - t)/(end - start)**2)**3

source = s.create(Cylinder, center=gs_highr* vec3(0.18, 0.13, 0.5), radius=(res_highr_x)* 0.04, z=gs_highr* vec3(0.02, 0, 0))

#print("----------------------obsPos info: initpos:", obsPos, "  obsSize:", obsSize, "  obsVelVec:", obsVelVec.x,",",obsVelVec.y , ",", obsVelVec.z )


buoyancy = vec3(0, 3e-4, 0)
sim_time=[]

#### Simulation step ####

def step(t):

    if not useReflectionMethod:
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
#        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))
        velocityInflow(flags=flags, vel=vel_highr, velToSet=vec3(1e-2, 0, 0) * float(res_highr_x), noise=noise2, shape=source, sigma=0.5) 
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)

        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        setWallBcs(flags=flags, vel=vel_highr)

    else:
        vel_highr_start.copyFrom(vel_highr)
        vel_highr_advector.copyFrom(vel_highr)
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
#        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))

        velocityInflow(flags=flags, vel=vel_highr, velToSet=vec3(1e-2, 0, 0) * float(res_highr_x), noise=noise2, shape=source, sigma=0.5)
        #advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)
        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # reflection: v = 2 P(v) - v, va = 2 P(v) - v0
        vel_highr_advector.copyFrom(vel_highr)
        solvePressure(flags=flags, vel=vel_highr_advector, pressure=pressure)
        vel_highr.multConst(vec3(-1.))
        vel_highr.addScaled(vel_highr_advector, vec3(2.))
        vel_highr_advector.multConst(vec3(2.))
        vel_highr_advector.addScaled(vel_highr_start, vec3(-1.))
        # half-step done
        s.step()
        t = t + 1
        # source
        if t<300:
            # source.applyToGrid(grid=density, value=1)
            densityInflow(flags=flags, density=density, noise=noise1, shape=source, scale=1, sigma=0.5)
#        density.save('%s/density_%04d.vol' % (mitsubaoutdir, t))

        # external forces
        setWallBcs(flags=flags, vel=vel_highr)
        addBuoyancy(density=density, vel=vel_highr, gravity=-buoyancy, flags=flags)
        # advection
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=density, order=2, orderSpace=2, orderTrace=2)
        advectSemiLagrange(flags=flags, vel=vel_highr_advector, grid=vel_highr, order=2, openBounds=True, boundaryWidth=bWidth, orderTrace=2)
        resetOutflow(flags=flags, real=density)


    if guide==0:
        setWallBcs(flags=flags, vel=vel_highr)
        solvePressure(flags=flags, vel=vel_highr, pressure=pressure)
    s.step()
    t = t + 1
    return t

#main loop
for t in range(num_frames):
        mantaMsg('\nFrame %i' % (s.frame))
        start = time.clock()
        t = step(t)

        if unguide_lowres:
                writeOutMACGrid(folderName=guide_vel_dir, vel=vel_highr,res = gs_highr,frameNum=t,binary=False)
        elif guide:
                readInMACGrid(folderName=guide_vel_dir, vel=vel_lowr, frameNum=t, res=gs_highr, lowres=gs_lowr, binary=False)
                ideal_filtering_smoke_guiding(vel_lowres=vel_lowr,vel_highres=vel_highr, lowres=gs_lowr,highres=gs_highr,cutoff=cutoff, bw = bWidth)
                setWallBcs(flags=flags, vel=vel_highr)
                solvePressure(flags=flags, vel=vel_highr, pressure=pressure)

        end = time.clock()
        sim_time.append((end-start))

        if 1:
            projectPpmFull( density, output_ppm % t , 0, 1.0 );

ts = float("%.2f"% (sum(sim_time) / float(len(sim_time))))


print('[resx resy resz  cutoff tg ts ]',res_highr_x,res_highr_y,res_highr_z, cutoff, ts)
file = open(output_runtime+"/meanTimes_buoyantJet2d_"+str(res_highr_x)+"_"+str(cutoff)+".txt","w")
file.write("resx, resy, resz, cutoff: ts "+str(res_highr_x)+", "+str(res_highr_y)+", "+ str(cutoff)+": "+str(ts))
file.close()

#fob = open("%s/obstSize.txt" % mitsubaoutdir,'w')
#fob.write("%5.4f" % (obsSize))
