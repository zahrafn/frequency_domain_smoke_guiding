#include "particle.h"
#include "grid.h"
#include "commonkernels.h"
#include "randomstream.h"

// Based on plugins/flip.cpp

using namespace std;
namespace Manta {


    PYTHON() void sampleDensityWithParticles(const Grid<Real>& density, const FlagGrid &flags, BasicParticleSystem &parts, const int discretization, const Real randomness) {
	const bool is3D = density.is3D();
	const Real jlen = randomness / discretization;
	const Vec3 disp (1.0 / discretization, 1.0 / discretization, 1.0/discretization);
	static RandomStream mRand(9832);
	FOR_IJK_BND(density, 0) {
            if (flags.isObstacle(i,j,k)) continue;
            if (!flags.isFluid(i,j,k)) continue;
            const Vec3 pos (i,j,k);
            for (int dk=0; dk<(is3D ? discretization : 1); dk++)
                for (int dj=0; dj<discretization; dj++)
                    for (int di=0; di<discretization; di++) {
                        Vec3 subpos = pos + disp * Vec3(0.5+di, 0.5+dj, 0.5+dk);
                        subpos += jlen * (Vec3(1,1,1) - 2.0 * mRand.getVec3());
                        if(!is3D) subpos[2] = 0.5; 
                        if(mRand.getReal() > density.getInterpolated(subpos)) continue; 
                        parts.addBuffered(subpos);
                    }
	}
	parts.insertBufferedParticles();
    }

    KERNEL(pts, single) template<class T>
    void knMapConst(const BasicParticleSystem& p, const FlagGrid& flags, Grid<T>& numer, Grid<Real>& denom, T value) {
	unusedParameter(flags);
	if (!p.isActive(idx)) return;
	numer.setInterpolated( p[idx].pos, value, denom);
    }

    PYTHON() void particlesToDensity (const FlagGrid& flags, Grid<Real>& density, const BasicParticleSystem& parts, const int discretization) {
	const bool is3D = flags.is3D();
        int ppc = discretization * discretization * (is3D ? discretization : 1); // number of particles per cell
	Grid<Real> tmp(flags.getParent());
	density.clear();
	knMapConst<Real>(parts, flags, density, tmp, 1./ppc);
    }



    PYTHON() void gridWithParticles(const Grid<Real>& grid, const FlagGrid &flags, BasicParticleSystem &parts, const int ibuff, const int jbuff , const int num_part_x, const int num_part_y) {
	
	float dw_x = grid.getSizeX()-(2*ibuff); //dd = 3 for the part with particle located in 1/3 of the whole sx
	float dw_y = grid.getSizeY()-(2*jbuff);
	float dx = (float)dw_x/num_part_x;
	float dy = (float)dw_y/num_part_y;

	int pz = grid.getSizeZ()/2;
	for(int i=0; i<num_part_x; i++){
		float px = ibuff + i*dx;
		for(int j=0; j<num_part_y; j++){
			float py = jbuff + j*dy;
			Vec3 pos(px,py,pz);
			parts.addBuffered(pos);		
		}	
	}
	parts.insertBufferedParticles();
    }


    PYTHON() void writeOutParticles(string fileName, BasicParticleSystem &parts, const MACGrid& vel) {

//        std::cout << "writing file: " << fileName << endl;

        FILE *file;
        string fileNameOut = fileName;
        const char *fileN = fileNameOut.c_str();
        file = fopen(fileN, "w");
        Grid<int> tmp( vel.getParent() );
        
        for (IndexInt idx=0; idx<(int)parts.size(); idx++) {
                if (parts.isActive(idx)) {
                        Vec3 p = parts.getPos(idx) ; //p is the position in i,j,k 
			//std::cout<< "-------part:"<< p << std::endl;
                        if (!tmp.isInBounds(p) ) {
                                parts.kill(idx); // out of domain, remove
                                continue;
                        }
			//fwrite(&vel(idx).x, sizeof(Real), 1, file)
			fprintf(file, "%.4f, %.4f, %.4f", p[0],p[1],p[2]);
			fprintf(file, "\n");
		}
        }
        fclose (file);

    }    


} // namespace

