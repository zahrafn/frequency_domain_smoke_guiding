/**
copyright 2020 Zahra Forootaninia
**/

#include "manta.h"
#include "vectorbase.h"
#include "interpol.h"
#include "interpolHigh.h"
#include "kernel.h"
#include "grid.h"
#include <fftw3.h>


using namespace std;
namespace Manta {

class FFTVec{
public:
        //constructor 
        FFTVec(MACGrid& macGrid, Vec3i res, int bw, int dir):
        
	        mmacGrid(macGrid), msize(res), mbw(bw), mdir(dir){
		mbw = mbw+1; // there is always one cell from each sides that is considered as boundary even if bw = 0
		mbx = res.x - mbw;
		mby = res.y - mbw;
		
                mxx = 0;
		myy = 0;
		mzz = 0;
		if(dir==0) mxx = 1;
		if(dir==1) myy = 1;
		if(dir==2) mzz = 1;
		
		msizex = res.x-2*mbw - mxx;
		msizey = res.y-2*mbw - myy;
		if(macGrid.is3D()){
                        mbz = res.z - mbw;
			msizez = msize.z-2*mbw - mzz;
                }
                else{
                        mbz = 1;
			msizez = 1;
                }

		mvalues.resize((msizex) * (msizey) * (msizez));
        };      

        


	void grid2fftVec_fftVec2grid(bool get){
        	
		FOR_IJK(mmacGrid){
			int idx = i + mmacGrid.getSizeX() * (j + mmacGrid.getSizeY() *k);
			if((mmacGrid).is3D()){
				if(k>=mbw + mzz && k<mbz && j>=mbw + myy && j<mby && i>=mbw + mxx && i<mbx){
					int idx_arr = (k-mbw-mzz) + msizez * ( (j-mbw-myy) + msizey * (i-mbw-mxx));
					if(get){
						mvalues[idx_arr] = mmacGrid(idx)[mdir];
					} 
					else{
						mmacGrid(idx)[mdir] = mvalues[idx_arr];
					}
				}
				else if( !get && (k<mbw + mzz || k>=mbz || j<mbw + myy || j>=mby || i<mbw + mxx || i>=mbx)){
					mmacGrid(idx)[mdir] = 0.0;
				}
			}

		 	if(!(mmacGrid).is3D()){
				 if(j>=mbw + myy && j<mby && i>=mbw + mxx && i<mbx){
                                        int idx_arr = (j-mbw-myy) + msizey * (i-mbw-mxx);
                                        if(get){
                                                mvalues[idx_arr] = mmacGrid(idx)[mdir];
                                        }       
                                        else{   
                                                mmacGrid(idx)[mdir] = mvalues[idx_arr];
                                        }
                                }
                                else if( !get && (j<mbw + myy || j>=mby || i<mbw + mxx || i>=mbx)){
                                       mmacGrid(idx)[mdir] = 0.0;
                                }
			}

		}
	

	}
	
	void grid2fftVec(){
		grid2fftVec_fftVec2grid(true);
	}

	void fftVec2grid(){
                grid2fftVec_fftVec2grid(false);
        }
		

	void fft() {
		
		double *ptrd = &mvalues[0];
		
		fftw_plan fftplan;
		if(mmacGrid.is3D()){
                        if(mdir == 0) fftplan = fftw_plan_r2r_3d( msizex, msizey, msizez, ptrd, ptrd, FFTW_RODFT00, FFTW_REDFT10, FFTW_REDFT10 ,FFTW_ESTIMATE);
                        if(mdir == 1) fftplan = fftw_plan_r2r_3d( msizex, msizey, msizez, ptrd, ptrd, FFTW_REDFT10, FFTW_RODFT00, FFTW_REDFT10 ,FFTW_ESTIMATE);
                        if(mdir == 2) fftplan = fftw_plan_r2r_3d( msizex, msizey, msizez, ptrd, ptrd, FFTW_REDFT10, FFTW_REDFT10, FFTW_RODFT00 ,FFTW_ESTIMATE);
                }
                else{
                        if(mdir == 0) fftplan = fftw_plan_r2r_2d( msizex, msizey, ptrd, ptrd, FFTW_RODFT00, FFTW_REDFT10 ,FFTW_ESTIMATE);
                        if(mdir == 1) fftplan = fftw_plan_r2r_2d( msizex, msizey, ptrd, ptrd, FFTW_REDFT10, FFTW_RODFT00 ,FFTW_ESTIMATE);
                }	
	
		fftw_execute(fftplan);

                int total_size = 4 * (msizex + mxx) * (msizey + myy);
				
                if(mmacGrid.is3D()) total_size = 8 * (msizex + mxx ) * (msizey + myy) * (msizez + mzz);
               
                for(int i=0; i< mvalues.size(); i++){
                        mvalues[i] /= total_size;
                }

		fftw_destroy_plan(fftplan);
	};

	void ifft() {
		
		double *ptrd = &mvalues[0];
		
		fftw_plan ifftplan;
		if(mmacGrid.is3D()){
                        if(mdir == 0) ifftplan = fftw_plan_r2r_3d(  msizex, msizey, msizez, ptrd, ptrd, FFTW_RODFT00, FFTW_REDFT01, FFTW_REDFT01 ,FFTW_ESTIMATE);
                        if(mdir == 1) ifftplan = fftw_plan_r2r_3d(  msizex, msizey, msizez, ptrd, ptrd, FFTW_REDFT01, FFTW_RODFT00, FFTW_REDFT01 ,FFTW_ESTIMATE);
                        if(mdir == 2) ifftplan = fftw_plan_r2r_3d(  msizex, msizey, msizez, ptrd, ptrd, FFTW_REDFT01, FFTW_REDFT01, FFTW_RODFT00 ,FFTW_ESTIMATE);
                }
                else{
                        if(mdir == 0) ifftplan = fftw_plan_r2r_2d( msizex, msizey, ptrd, ptrd, FFTW_RODFT00, FFTW_REDFT01 ,FFTW_ESTIMATE);
                        if(mdir == 1) ifftplan = fftw_plan_r2r_2d( msizex, msizey, ptrd, ptrd, FFTW_REDFT01, FFTW_RODFT00 ,FFTW_ESTIMATE);
                }
		fftw_execute(ifftplan);
		fftw_destroy_plan(ifftplan);

	};


	
	void apply_ideal_filter(FFTVec &lr_fftVec,double cutoff){

                float scale = 0.0;
                if(mdir == 0) scale = float (msize.x) / (lr_fftVec.msize.x);
                if(mdir == 1) scale = float (msize.y) / (lr_fftVec.msize.y);
                if(mdir == 2) scale = float (msize.z) / (lr_fftVec.msize.z);
		int filtr_num = 0;		
                for(int k=0 ,__kmax=((mmacGrid).is3D() ? lr_fftVec.msizez : 1); k<__kmax; k++){
                        for(int j=0 ; j<lr_fftVec.msizey; j++){
                                for(int i=0 ; i<lr_fftVec.msizex; i++){
                                       		
                                        int idx_arr_low = k + lr_fftVec.msizez * (j + lr_fftVec.msizey * i);
					int idx_arr_high = k + msizez * (j + msizey * i);
					
					
					float dist = pow((float)(i+mxx)/(lr_fftVec.msizex+mxx), 2) + pow((float)(j+myy)/(lr_fftVec.msizey+myy), 2) + pow((float)(k+mzz)/(lr_fftVec.msizez+mzz), 2);
					if (dist < pow(cutoff, 2)){
						mvalues[idx_arr_high] = lr_fftVec.mvalues[idx_arr_low] * scale;
                                        }
					
                                }
                        }
                }
	};

	
	void upsample(FFTVec &lr_fftVec){

                float scale = 0.0;
                if(mdir == 0) scale = float (msize.x) / (lr_fftVec.msize.x);
                if(mdir == 1) scale = float (msize.y) / (lr_fftVec.msize.y);
                if(mdir == 2) scale = float (msize.z) / (lr_fftVec.msize.z);
		int filtr_num = 0;		
                for(int k=0 ,__kmax=((mmacGrid).is3D() ? lr_fftVec.msizez : 1); k<__kmax; k++){
                        for(int j=0 ; j<lr_fftVec.msizey; j++){
                                for(int i=0 ; i<lr_fftVec.msizex; i++){
                                       		
                                        int idx_arr_low = k + lr_fftVec.msizez * (j + lr_fftVec.msizey * i);
					int idx_arr_high = k + msizez * (j + msizey * i);
					
					
					float dist = pow((float)(i+mxx)/(lr_fftVec.msizex+mxx), 2) + pow((float)(j+myy)/(lr_fftVec.msizey+myy), 2) + pow((float)(k+mzz)/(lr_fftVec.msizez+mzz), 2);
					if (dist < 1){
						mvalues[idx_arr_high] = lr_fftVec.mvalues[idx_arr_low] * scale;
                                        }
					else
						mvalues[idx_arr_high] = 0.0;
					
                                }
                        }
                }
	};


	void apply_gaussian_filter(FFTVec &lr_fftVec,double cutoff){
                float scale = 0.0;
                if(mdir == 0) scale = float (msize.x) / (lr_fftVec.msize.x);
                if(mdir == 1) scale = float (msize.y) / (lr_fftVec.msize.y);
                if(mdir == 2) scale = float (msize.z) / (lr_fftVec.msize.z);
		int filtr_num = 0;		
                for(int k=0 ,__kmax=((mmacGrid).is3D() ? lr_fftVec.msizez : 1); k<__kmax; k++){
                        for(int j=0 ; j<lr_fftVec.msizey; j++){
                                for(int i=0 ; i<lr_fftVec.msizex; i++){
                                       		
                                        int idx_arr_low = k + lr_fftVec.msizez * (j + lr_fftVec.msizey * i);
					int idx_arr_high = k + msizez * (j + msizey * i);
					
					
					float dist = pow((float)(i+mxx)/(lr_fftVec.msizex+mxx), 2) + pow((float)(j+myy)/(lr_fftVec.msizey+myy), 2) + pow((float)(k+mzz)/(lr_fftVec.msizez+mzz), 2);
					if (dist < pow(cutoff, 2)){
						float w_gf = exp(-dist/(cutoff*cutoff));
						mvalues[idx_arr_high] = w_gf*lr_fftVec.mvalues[idx_arr_low] * scale + (1-w_gf)*mvalues[idx_arr_high];
                                        }
					
                                }
                        }
                }
	};






//protected:

        Vec3i msize; //resolution of the grid
        MACGrid &mmacGrid;
        int mbw; //boundary width
        int msizex, msizey, msizez; //size inside boundary x, y and z dir of fftVec
	int mbx, mby, mbz; //last element inside boundary of x, y and z dir of MAC
	int mxx, myy, mzz; // 1 if we skip the first element inside boundry for taking fft and 0 if we take all elemnts 
        int mdir; // 0->x , 1->y, 2->z
	vector<double> mvalues; // fft vectors
};





};
