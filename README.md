This is an extension to the Mantaflow fluid simulator. Our method of smoke guiding is added in fluidguiding.cpp file under manta-src-0.12/manta_0_12/sources/plugin and the fftVec.h is required to run the guiding methods. fftVec.h is under manta-src-0.12/manta_0_12/sources/.

After compiling Mantaflow. You can run most of scenarios such as :

./manta SCNERAIO_NAME RES_X RES_Y RES_Z FLAG_LOWRES FLAG_GUIDE CUTOFF_INV NUM_FRAMES SCALE

FLAG_LOWRES = 1 for running low resolution simulation which will be used as guiding input
FLAG_GUIDE = 1 if guiding is going to be applied
CUTOFF_INV = inverse of the low/high pass filters
SCALE = upresing scale (high_res/low_res)

For example for Sphere scenario:

1. Run the low resolution simulation first which will be used as guide velocities for the high res simulation.
./manta ../scenes/sphere.py 16 32 16 1 0 0 100 1

2. Run the guided high-res simulation with cutoff of 1/4 and upres scale of 3
./manta ../scenes/sphere.py 16 32 16 0 1 4 100 3


NOTE: plume_pd_upsampl_gaussflt.py scenario requires more inputs which can be found at the beginning of the python script.   

